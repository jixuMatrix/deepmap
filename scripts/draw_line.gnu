set term eps
set output "5pp_footprint.eps"


set ylabel "Footprint (MB)" font ",20"
set xlabel "Normalized lifetime" font ",20"
plot "5app.dat" using 2:xticlabels(1) title columnheader(4) w lp linecolor rgb "#FFFFFF" pt 1, \
'' using 3:xticlabels(1) title columnheader(3) w lp linecolor red pt 2, \
'' using 4:xticlabels(1) title columnheader(2) w lp linecolor rgb "#BEBEBE" pt 3
'' using 5:xticlabels(1) title columnheader(5) w lp linecolor rgb "#90EE90" pt 7, \
'' using 6:xticlabels(1) title columnheader(6) w lp linecolor rgb "#FF0000" pt 5

