set term eps enhanced
set output outputfile
set style data histogram
set style histogram clustered gap 1
set style fill solid 1 border 0
set style histogram rowstacked
#set key font ",24" width 2.5
#set key horizontal
#set style fill pattern 0  border 1

#set key top center
set key at 17, 12
#set key top
#set yrange [0:11.8]
set yrange [0:11]
set xtics font ",15"
set xlabel font ",15"
set key font ",15"
set key invert
#set key reverse

#set style histogram rowstacked
set boxwidth 0.8 relative
set xtics rotate by -45
plot inputfile using 2:xticlabels(1) title columnheader(2) linecolor rgb "#000000", \
'' using 3:xticlabels(1) title columnheader(3) linecolor rgb "#FFFFFF", \
'' using 4:xticlabels(1) title columnheader(4) linecolor rgb "#BEBEBE", \
'' using 5:xticlabels(1) title columnheader(5) linecolor rgb "#006600" lt -1 fillstyle pattern 2, \
'' using 6:xticlabels(1) title columnheader(6) linecolor rgb "#000099" lt -1 fillstyle pattern 7;

