import sys
import math

sizes = []
lifetimes = []
max_size = 0.0
vars = []

major_file = open("./Cate", "r")
line = major_file.readline()
while line:
    vars.append(int(line.split()[0]))
    line = major_file.readline()

file = open("./var_result", "r")
line = file.readline()
#outfile = open("/home/xji/sds/" + sys.argv[1] + "_sd", "w")
outfile = open("/home/xji/marjor_count_sd", "a")
#outfile.write( "Counts:" + str(len(vars)) + "\n")
while line:
    if "What" in line:
        line = file.readline()
        continue
    if "footprint" in line:
        line = file.readline()
        continue
    var_id = int(line.split()[0])
    flag = 0
    #for i in range(0, len(vars)):
    #    if var_id == vars[i]:
    #        flag = 1
    #if flag == 0:
    #    line = file.readline()
    #    continue
    #print vars
    avg_size = float(line.split()[2])
    variance = float(line.split()[4])
    counts = int(line.split()[1])
    sd = (variance * avg_size / 100) * (variance * avg_size / 100)
    sd = sd / counts
    sd = math.sqrt(sd)
    cv = sd / avg_size

    #print var_id, counts
    #if counts > 10000:
    #    outfile

    outfile.write( str(counts) + "\t" + str(cv) + "\n")

    line = file.readline()
