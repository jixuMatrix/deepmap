set term eps enhanced
set output outputfile
set style data histogram
set style histogram clustered gap 1
set style fill solid 1 border 0
 
#set style fill pattern 0  border 1
set xlabel "Variable ID\\_Object ID" font ",17"
set ylabel "Density(S^{-1})" font ",14"
#set title "Volume per page of bzip2"
set logscale y
set format y "%.0e"
set key left

#set style histogram rowstacked
set boxwidth 0.8 relative
set xtics rotate by -45
plot inputfile using 2:xticlabels(1) title columnheader(2) linecolor rgb "#000000", \
'' using 3:xticlabels(1) title columnheader(3) linecolor rgb "#FFFFFF", \
'' using 4:xticlabels(1) title columnheader(4) linecolor rgb "#BEBEBE"
#'' using 5:xticlabels(1) title columnheader(5) linecolor rgb "#90EE90", \
#'' using 6:xticlabels(1) title columnheader(6) linecolor rgb "#FF0000"
