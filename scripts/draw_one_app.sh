#!/bin/bash

echo $1

gnuplot -e "inputfile='../results/$1/Size.dat'; outputfile='../results/$1/$1_size.eps'" draw_size.gnu
#gnuplot -e "inputfile='../results/$1/touches_perpage.dat'; outputfile='../results/$1/$1_touches_perpage.eps'" draw_touches_perpage.gnu
#gnuplot -e "inputfile='../results/$1/Total_access_volume_bytes.dat'; outputfile='../results/$1/$1_volume.eps'" draw_volume.gnu
#gnuplot -e "inputfile='../results/$1/reference_pertouch.dat'; outputfile='../results/$1/$1_reference_pertouch.eps'" draw_volume_pertouch.gnu
#gnuplot -e "inputfile='../results/$1/reference_perpage.dat'; outputfile='../results/$1/$1_reference_perpage.eps'" draw_reference_perpage.gnu
#gnuplot -e "inputfile='../results/$1/dirty.dat'; outputfile='../results/$1/$1_dirty.eps'" draw_dirty.gnu
gnuplot -e "inputfile='../results/$1/read_ratio_volume.dat'; outputfile='../results/$1/$1_read.eps'" draw_read_ratio.gnu
gnuplot -e "inputfile='../results/$1/seq_ratio_volume.dat'; outputfile='../results/$1/$1_seq.eps'" draw_seq_ratio.gnu
#gnuplot -e "inputfile='../results/$1/stride_ratio_volume.dat'; outputfile='../results/$1/$1_stride.eps'" draw_stride_ratio.gnu
#gnuplot -e "inputfile='../results/$1/Avg_offset_bytes.dat'; outputfile='../results/$1/$1_avg_offset.eps'" draw_avg_dis_in_access.gnu
#gnuplot -e "inputfile='../results/$1/access_bytes_size.dat'; outputfile='../results/$1/$1_density.eps'" draw_density.gnu
gnuplot -e "inputfile='../results/$1/access_bytes_size.dat'; outputfile='../results/$1/$1_density_no_time.eps'" draw_density_no_time.gnu
gnuplot -e "inputfile='../results/$1/Temporal_locality_page.dat'; outputfile='../results/$1/$1_tem_loc_page.eps'" draw_temperal_locality_page.gnu
gnuplot -e "inputfile='../results/$1/Spatial_locality_page.dat'; outputfile='../results/$1/$1_spa_loc_page.eps'" draw_spatial_locality_page.gnu
gnuplot -e "inputfile='../results/$1/Temporal_locality_cache.dat'; outputfile='../results/$1/$1_tem_loc_cache.eps'" draw_temperal_locality_cache.gnu
gnuplot -e "inputfile='../results/$1/Spatial_locality_cache.dat'; outputfile='../results/$1/$1_spa_loc_cache.eps'" draw_spatial_locality_cache.gnu
