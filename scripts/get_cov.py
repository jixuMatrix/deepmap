# Summarize the var_size_file which contaions all the allocation informations!

import sys
import math
raw_file = open("./var_size_file", "r")

line = raw_file.readline()

obj_address = {}
obj_size = {}
obj_time = {}
var_sets = {}
var_footprint = {}
var_totaltime = {}
obj_var = {}
max_var_footprint = {}
total_obj = 0
var_starttime = {}
var_endtime = {}
obj_flag = {}
last_time = 0.0
var_count = {}
var_livetime = {}
total_footprint = 0
max_total_footprint = 0
var_ids = []


def HandleAlloc(obj_id, var_id, ptr, size, time):
    global total_obj
    global total_footprint
    global max_total_footprint
    obj_address[ptr] = total_obj
    obj_size[total_obj] = size
    obj_var[total_obj] = var_id
    obj_time[total_obj] = time
    obj_flag[total_obj] = 1
    
    if var_id in var_sets:
        var_sets[var_id].append(size)
    else:
        var_sets[var_id] = []
        var_sets[var_id].append(size)
        
    if var_id in var_footprint:
        var_footprint[var_id] += size
    else:
        var_footprint[var_id] = size

    total_footprint += size
    if total_footprint < max_total_footprint:
        max_total_footprint = total_footprint

    if var_id not in max_var_footprint:
        max_var_footprint[var_id] = size
    elif var_footprint[var_id] > max_var_footprint[var_id]:
        max_var_footprint[var_id] = var_footprint[var_id]

    if var_id not in var_count:
        var_count[var_id] = 1
        var_livetime[var_id] = 0.0
    else:
        var_count[var_id] += 1
        
    if var_count[var_id] == 1:
        var_starttime[var_id] = time
        
    total_obj = total_obj + 1

def HandleFree(ptr, time):
    global total_footprint
    global max_total_footprint
    try:
        var_id = obj_var[obj_address[ptr]]
        obj_id = obj_address[ptr]
    except:
        print "What alloc or free?", ptr
        return
    
    var_footprint[var_id] -= obj_size[obj_address[ptr]]
    total_footprint -= obj_size[obj_address[ptr]]
    obj_size[obj_address[ptr]] = 0

    if var_id in var_totaltime:
        var_totaltime[var_id] += \
                time - obj_time[obj_address[ptr]]
    else: 
        var_totaltime[var_id] = \
                time - obj_time[obj_address[ptr]]

    #print "aaaa", var_id, var_totaltime[var_id], time, obj_time[obj_id]
    
    var_count[var_id] -= 1

    if var_count[var_id] == 0:
        var_livetime[var_id] += time - var_starttime[var_id]

    obj_flag[obj_address[ptr]] = 0

def HandleRealloc(pre_ptr, ptr, size, time):
    global total_footprint
    global max_total_footprint

    try:
        var_id = obj_var[obj_address[pre_ptr]]
        obj_id = obj_address[pre_ptr]
    except:
        print "Realloc: What alloc or free?", ptr
        return
        
    var_footprint[var_id] -= obj_size[obj_address[pre_ptr]]
    total_footprint -= obj_size[obj_address[pre_ptr]]

    obj_size[obj_id] = size
    obj_address[ptr] = obj_id
    
    if var_id in var_sets:
        var_sets[var_id].append(size)
    if var_id in var_footprint:
        var_footprint[var_id] += size
    
    total_footprint += size
    if total_footprint < max_total_footprint:
        max_total_footprint = total_footprint

    if var_footprint[var_id] > max_var_footprint[var_id]:
        max_var_footprint[var_id] = var_footprint[var_id]


while line:
    if "malloc" in line:
        obj_id = int(line[line.find("malloc:") + 7:line.find("var_id")])
        var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
        ptr = line[line.find("ptr:") + 4:line.find("size")]
        size = int(line[line.find("size:") + 5:line.find("times")])
        time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
        HandleAlloc(obj_id, var_id, ptr, size, time)

    if "calloc" in line:
        obj_id = int(line[line.find("calloc:") + 7:line.find("var_id")])
        var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
        ptr = line[line.find("ptr:") + 4:line.find("size")]
        size = int(line[line.find("size:") + 5:line.find("times")])
        time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
        HandleAlloc(obj_id, var_id, ptr, size, time)

    if "realloc" in line:
        obj_id = int(line[line.find("realloc:") + 8:line.find("var_id")])
        var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
        ptr = line[line.find("ptr:") + 4:line.find("before")]
        size = int(line[line.find("size:") + 5:line.find("times")])
        pre_ptr = line[line.find("before:") + 7:line.find("size")]
        time = float(line[line.find("times:") + 6:].replace('\n', ''))
        #if size == 0:
        #    HandleFree(pre_ptr, time)
        #else:
        #    HandleRealloc(pre_ptr, ptr, size, time)

        HandleFree(pre_ptr, time)
        HandleAlloc(obj_id, var_id, ptr, size, time)
    if "free" in line:
        ptr = line[line.find("ptr:") + 4:line.find("times")]
        time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
        HandleFree(ptr, time)

    if "times:" in line:
        last_time = float(line[line.find("times:") + 6:].replace('\n', ''))

    line = raw_file.readline()

#deal with the unfree malloc

for i in range(0, len(obj_flag)):
    if obj_flag[i] == 1:
        var_id = obj_var[i]
        
        var_count[var_id] -= 1

        if var_count[var_id] == 0:
            var_livetime[var_id] += time - var_starttime[var_id]

        if var_id in var_totaltime:
            var_totaltime[var_id] += \
                last_time - obj_time[i]
        else: 
            var_totaltime[var_id] = \
                last_time - obj_time[i]

#print "Var_ID", "#_of_objects", "Avg", "Footprint", "Varivance_%", "Avg_#_of_concurrent"

for i in range(0, len(var_sets)):
    total_size = 0
    min_size = 1e20
    max_size = 0
    if i+1 not in var_sets:
        continue
    for j in range(0, len(var_sets[i+1])):
        total_size += var_sets[i+1][j]
        min_size = min(var_sets[i+1][j], min_size)
        max_size = max(var_sets[i+1][j], max_size)
    avg = float(total_size) / len(var_sets[i+1])
    sd = 0.0
    for j in range(0, len(var_sets[i+1])):
        sd += (var_sets[i+1][j] - avg) * (var_sets[i+1][j] - avg)
    sd = math.sqrt(sd)
    
    #print var_totaltime[i+1]
    #print var_starttime[i+1]
    #print var_endtime[i+1]
    print i+1, len(var_sets[i+1]), avg, max_var_footprint[i+1],  sd * 100 / avg, var_totaltime[i+1] / var_livetime[i+1]
    #print "total footprint", total_footprint




