"""
Demo of a basic pie chart plus a few additional features.

In addition to the basic pie chart, this demo shows a few optional features:

    * slice labels
    * auto-labeling the percentage
    * offsetting a slice with "explode"
    * drop-shadow
    * custom start angle

Note about the custom start angle:

The default ``startangle`` is 0, which would start the "Frogs" slice on the
positive x-axis. This example sets ``startangle = 90`` such that everything is
rotated counter-clockwise by 90 degrees, and the frog slice starts on the
positive y-axis.
"""
import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.rcParams['font.size'] = 10.0
# The slices will be ordered and plotted counter-clockwise.
#labels = 'Frogs', 'Hogs', 'Dogs', 'Logs'
labels = '1D-Hashtable', '1D-buffer', '1D-vector', '1D-bitmap', '1D-index', '1D-other', '2D-matrix', '0D-structure', 'unknown'
sizes = [15, 30, 45, 10]
colors = ['red', 'lightgrey', 'blue', 'pink', 'yellowgreen', 'gold', 'lightskyblue', 'lightcoral', 'magenta', 'teal']
explode = (0, 0, 0, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')
count_numbers=[14, 38, 93, 6, 7, 25, 23, 21 ,45]
size_numbers=[261771680, 10244541611, 143868299978, 341722512, 5276409269, 753816844, 3647664608, 22834798160, 76591211040]
footprint_numbers=[256114675.5, 60451038658, 148532173167, 226416286.7, 1753969361, 25063036593, 4158210638, 11055541149, 88194693042]


#plt.pie(sizes, explode=explode, labels=labels, colors=colors,
#        autopct='%1.1f%%', shadow=True, startangle=90)
# Set aspect ratio to be equal so that pie is drawn as a circle.
#plt.axis('equal')

patterns = [ "/" , "\\" , "|" , "-" , "+" , "x", "o", "O", ".", "*" ]
fig = plt.figure(1, figsize=(9,3.3))
ax = fig.gca()
import numpy as np
#patches, texts = plt.pie(sizes, colors=colors, startangle=90, radius=0)
piechart = ax.pie(count_numbers, colors=colors,
       autopct='%1.0f%%', startangle=0,
       radius=0.4, center=(0, 0.5), frame=True)
for i in range(len(piechart[0])):
    piechart[0][i].set_hatch(patterns[(i)%len(patterns)])

piechart = ax.pie(size_numbers, colors=colors,
       autopct='%1.0f%%', startangle=0,
       radius=0.4, center=(1, 0.5), frame=True)
for i in range(len(piechart[0])):
    piechart[0][i].set_hatch(patterns[(i)%len(patterns)])

piechart = ax.pie(footprint_numbers, colors=colors,
       autopct='%1.0f%%', startangle=0,
       radius=0.4, center=(2, 0.5), frame=True)
for i in range(len(piechart[0])):
    piechart[0][i].set_hatch(patterns[(i)%len(patterns)])

#patches, texts = plt.pie(sizes, colors=colors, startangle=90, radius=0, center=(0, 0.5))

plt.legend(labels, loc="upper center", ncol=5)

ax.set_xticks([0, 1, 2])
#ax.set_yticks([0, 1])
ax.set_xticklabels(["Count", "Total Size", "Footprint"])
#ax.set_yticklabels(["Dry", "Rainy"])
ax.set_xlim((-0.5, 2.5))
ax.set_ylim((0.1, 1.2))
ax.get_yaxis().set_visible(False)
# Set aspect ratio to be equal so that pie is drawn as a circle.
ax.set_aspect('equal')

plt.savefig("./distribution.eps", format="eps")
plt.show()
