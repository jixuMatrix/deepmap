import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from numpy.random import randn
import numpy as np
import struct

start_time = []
color = []
axs = []
var_total = 28
fig = plt.figure()
draw_list = [1, 2, 3, 4, 5, 10, 13, 18, 21, 26]


read_time = []
write_time = []
read_offset = []
write_offset = []
start_time = 0

#plt.plot (randn(50).cumsum(), 'k')
def init_color ():
    color.append("#0000FF")
    color.append("#8A2BE2")
    color.append("#A52A2A")
    color.append("#5F9EA0")
    color.append("#7FFF00")
    color.append("#000000")
    
    
def plt_one (time_string, offset_string):

    time, = struct.unpack("I", time_string)
    offset, = struct.unpack("I", offset_string)
    time = long(time) + (long(offset & 0xe0000000) << 3)
    operation = (offset & 0x10000000) 
    offset = long(offset & 0x0fffffff)
    #print(type(time))
    #print(type(offset)) 
    if operation > 0:
            #plt.scatter(time - start_time[name_index-1], offset, color=color[name_index-1] )
            #axs[name_index-1].plot((time - start_time[name_index-1])/1e6, offset, '.', color=color[name_index-1], markersize=5)
        write_time.append(time / 1e6)
        write_offset.append(offset)
    else:
            #plt.scatter(time - start_time[name_index-1], offset, color=color[(name_index-1)*2] )
            #axs[name_index-1].plot((time - start_time[name_index-1])/1e6, offset, '.', color=color[(name_index-1)+var_total], markersize=5)
        read_time.append(time / 1e6)
        read_offset.append(offset)

def draw_pic():
    for i in range(0, var_total):
        plt.plot(write_time, write_offset, '.',  color=color[1], markersize=2)
        plt.plot(read_time, read_offset, '.', color=color[2], markersize=2)
        
def add_label():
    b1 = plt.bar([0], [0], width=0.4, color=color[2], label="read")
            #patch = mpatches.Patch(color=color[i], label=str(var_num)+"_read")
    b1 = plt.bar([0], [0], width=0.4, color=color[1], label="write")
        #plt.legend(handles=[patch],frameon='false', loc="upper right", shadow="true", mode="expand", fancybox="true")
    plt.legend(loc='best', fancybox=True, shadow=True)

init_color()
for i in range(0, var_total):
    print(i)
    if (i+1) not in draw_list: continue
    f = open("mm-trace-file_" + str(i+1), 'rb')
    read_time = []
    write_time = []
    read_offset = []
    write_offset = []
    start_time = 0

    #line = f.readline()
    #while line:
    #    plt_one(line)
    #    line = f.readline()
    time_string = f.read(4)
    offset_string = f.read(4)
	
    while time_string:
        plt_one(time_string, offset_string)
        time_string = f.read(4)
        offset_string = f.read(4)

    draw_pic()
    add_label()
    plt.xlabel("Time(s)")
    plt.ylabel("Access offset (Page)")
#plt.legend(loc="best", shadow="true", bbox_to_anchor=(1, 1))
    #plt.show()
    plt.savefig('trace_' + str(i+1) + '.png', dpi=1000 )
    plt.clf()
