set term eps
set output "read_episode.eps"

set format y "%g%%"
set ytics font ",18"
set xtics font ",20"
set ylabel "Read ratio" font ",22" offset -2
set xlabel "Episode" font ",24"
set key left font ",16"
#spacing 1.6 at 0.2, 2400
set xrange [0:185]
#set xtics 0, .2, 1
#set xtics [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0,7, 0.8, 0.9, 1.0]
set lmargin at screen 0.18
set bmargin at screen 0.2


plot "bzip2_read_episode.dat" using 4 title columnheader(4) w lines linecolor rgb "#000000" lw 2, \
'' using 3 title columnheader(3) w lines linecolor rgb "blue" lw 2, \
'' using 2 title columnheader(2) w lines linecolor rgb "grey" lw 2, \
#'' using 1:6 title columnheader(6) w lines linecolor rgb "#0000FF" lw 2 pt 7 ps 0.5, \
#'' using 1:7 title columnheader(7) w lp linecolor rgb "#FF0000" lw 2 pt 5 ps 0.5
