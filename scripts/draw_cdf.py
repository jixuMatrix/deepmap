import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
import sys
import os
import struct

#print sys.argv
if len(sys.argv) != 4:
    print "Usage: script_name <source file directory> <result file directory> <type of figure>"
    print "type 1: cdf"
    print "type 2: total memory size"
    print "type 3: total count"
    print "type 4: life time"
    exit (0)

MAX_VARIABLE_SIZE = 8192

rootdir = sys.argv[1]
resultdir = sys.argv[2]
figure_type = int(sys.argv[3])
color = []
color_count = 0
start_time = 0.0
end_time = 0.0

def init_color ():
    color.append("#0000FF")
    color.append("#FFFFFF")
    color.append("#BEBEBE")
    color.append("#90EE90")
    color.append("#90EE90")
    color.append("#000000")

def cdf_plot(data, name, number):
   
    #print 'data'
    #print data 
    ecdf = sm.distributions.ECDF(data)
    x = np.linspace(min(data)-1, max(data)+1, number)
    y = ecdf(x)
    #print x
    #print y

    plt.plot(x, y, label=name)

def size_plot(time_data, size_data, label_name):
    global color_count
    plt.plot(time_data, size_data, '-',  color=color[color_count], markersize=4, label=label_name)
    color_count = color_count + 1

def count_plot(time_data, count_data, label_name):
    global color_count
    plt.plot(time_data, count_data, '-', color=color[color_count], markersize=4, label=label_name)
    color_count = color_count + 1


def GetFirstLastAcccessTime(first_access_times, last_access_times, dirname):
    i = 0
    global start_time
    global end_time
    while 1: 
        if not os.path.isfile(rootdir + "/" + dirname + "/mm-trace-file_" + str(i + 1)):
            break
        f = open(rootdir + "/" + dirname + "/mm-trace-file_" + str(i + 1), 'rb')
        time_string = f.read(4) # time in binary formant
        offset_string = f.read(4) #offset in binary format
        last_time_string = time_string
        last_offset_string = offset_string
        last2_time_string = time_string
        last2_offset_string = offset_string
        
        time, = struct.unpack("I", time_string)
        offset, = struct.unpack("I", offset_string)
        first_access_time = long(time) + (long(offset & 0xe0000000) << 3)
        #first_access_time = (time - start_time) / (end_time - start_time)
    
        print time / 1e6
        #print start_time - end_time
        #print first_access_time
        while time_string:
            last2_time_string = last_time_string
            last2_offset_string = last_offset_string
            
            last_time_string = time_string
            last_offset_string = offset_string
                        
            time_string = f.read(4)
            offset_string = f.read(4)
            
        time, = struct.unpack("I", last2_time_string)
        offset, = struct.unpack("I", last2_offset_string)
        last_access_time = long(time) + (long(offset & 0xe0000000) << 3)

        time, = struct.unpack("I", last_time_string)
        offset, = struct.unpack("I", last_offset_string)
        variable_end_time = long(time) + (long(offset & 0xe0000000) << 3)
        normalized_last_access_time = float(last_access_time) / variable_end_time
        normalized_first_access_time = float(first_access_time) / variable_end_time
        #last_access_time = (time - start_time) / (end_time - start_time)
        #print last_access_time
        #print time / 1e6
        print normalized_first_access_time, normalized_last_access_time
        first_access_times.append(normalized_first_access_time)
        last_access_times.append(normalized_last_access_time)

        i = i + 1
    
def lifetime_plot(start_times, end_times, first_access_times, last_access_times, size_array, sort_size_array, label_name):
    max_object_size = 0
    for i in range(0, len(start_times)):
        object_size = float(size_array[i]) / (1024 * 1024)
        sorted_id = 0
        #print sort_size_array
        for j in range(0, len(sort_size_array)):
            if i == sort_size_array[j][1]:
                sorted_id = j + 1
                break
        #print sorted_id 

        # for bzip2 
        '''
        if sorted_id == 27 or sorted_id == 28:
            continue
        if sorted_id <= 7:
            continue
        if sorted_id == 26:
            plt.text(0.7, 26.8, "x3" ,fontsize = 12, verticalalignment="top", horizontalalignment = "right")
            
        if sorted_id == 14 or sorted_id == 15 or sorted_id == 16:
            sorted_id = 16
          
        if sorted_id == 8 or sorted_id == 9 or sorted_id == 10:
            sorted_id = 14
            
        if sorted_id == 11 or sorted_id == 12 or sorted_id == 13:
            sorted_id = 15
        '''
        
        id = i + 1
        if id == 1:
            sorted_id = 1
        elif id == 2:
            sorted_id = 2
        elif id == 3:
            sorted_id = 3
        elif id == 7 or id == 15 or id == 23:
            sorted_id = 4
        elif id == 8 or id == 16 or id == 24:
            sorted_id = 5
        elif id == 12 or id == 20 or id == 28:
            sorted_id = 6
        else:
            continue
              
        #print sorted_id 
        max_object_size = max(max_object_size, object_size)
        first_access_time = first_access_times[i] * (end_times[i] - start_times[i]) + start_times[i]
        last_access_time = last_access_times[i] * (end_times[i] - start_times[i]) + start_times[i]
        #plt.plot([start_times[i], first_access_time], [sorted_id, sorted_id], '-', color='blue', linewidth = 3)
        #plt.plot([first_access_time, last_access_time], [sorted_id, sorted_id], '-', color='black', linewidth = 3)
        #plt.plot([last_access_time, end_times[i]], [sorted_id, sorted_id], '-', color='red', linewidth = 3)
        plt.barh(sorted_id, first_access_time - start_times[i], color = 'blue', left = start_times[i], height = 0.4)
        '''
        if id == 7 or id == 8 or id == 12:
            plt.barh(sorted_id, last_access_time - first_access_time, color = 'white', left = first_access_time, height = 0.4, hatch = '/')
        elif id == 15 or id == 16 or id == 20:
            plt.barh(sorted_id, last_access_time - first_access_time, color = 'white', left = first_access_time, height = 0.4, hatch = '\\')
        elif id == 23 or id == 24 or id == 28:
            plt.barh(sorted_id, last_access_time - first_access_time, color = 'white', left = first_access_time, height = 0.4, hatch = 'x')
        else:
            plt.barh(sorted_id, last_access_time - first_access_time, color = 'white', left = first_access_time, height = 0.4, hatch = '/')
        '''   
        plt.barh(sorted_id, last_access_time - first_access_time, color = 'white', left = first_access_time, height = 0.4)
        plt.barh(sorted_id, end_times[i] - last_access_time, color = 'black', left = last_access_time, height = 0.4)
        
                
        #plt.plot(start_times[i], object_size, '^', color = 'white', markersize = 5)
        #plt.plot(start_times[i], object_size, 'x', color = 'black', markersize = 5)
        #plt.plot(end_times[i], object_size, 's', color = 'white', markersize = 5)
        #plt.plot(end_times[i], object_size, '+', color = 'black', markersize = 5)
        
#        plt.plot([start_times[i], first_access_times[i], last_access_times[i], end_times[i]], [object_size, object_size, object_size, object_size], '-o', color='blue', markersize=3)
    plt.ylabel("Variable ID", fontsize = 24)
    #plt.ylim([0,int(max_object_size) + 1])
    plt.xlabel("Normalized execution time", fontsize = 24)
    #plt.legend(bbox_to_anchor=(0.6, 0.4), loc=2, borderaxespad=0.)
    plt.savefig(resultdir + "/" + label_name + "_lifetime.eps", format="eps")
    plt.clf()
        
    
def get_start_end_time(dirname):
    global start_time
    global end_time
    trace_file = open(rootdir + "/" + dirname + "/call-trace-mmap", "r")
    line = trace_file.readline()
    while line:
        if "[Start]" in line:
            loc = line.find("time") + len("time:")
            start_time = float(line[loc:])
        if "[End]" in line:
            time_start_loc = line.find("time") + len("time:")
            end_time = float(line[time_start_loc:])
        line = trace_file.readline()
    print dirname, start_time, end_time, end_time - start_time
    
def GetDataWork(dirname):
    total_size = 0
    alive_count = 0

    aggregate_size = []
    alive_num = []
    time_point = []
    ptr_array = []
    start_times = []
    end_times = []
    first_access_times = []
    last_access_times = []
    size_array = []
    alive_count_array = []
    sort_size_array = []
    id = 0
    
    trace_file = open(rootdir + "/" + dirname + "/call-trace-mmap", "r")
    line = trace_file.readline()
    while line:
        if "[Trace]" in line and "malloc" in line:
            loc1 = line.find("size") + len("size:")
            loc2 = line.find("times")
            loc3 = loc2 + len("times:")
            loc4 = line.find("ptr:") + len("ptr:")
            loc5 = line.find("size:")
            
            var_length = int(line[loc1:loc2])
            
            time = (float(line[loc3:]) - start_time) / (end_time - start_time)
            ptr = line[loc4:loc5]
            
            total_size += var_length
            alive_count += 1

            time_point.append(time)
            aggregate_size.append( float(total_size) / (1024 * 1024))
            alive_count_array.append(alive_count)
            ptr_array.append(ptr)
            start_times.append(time)
            end_times.append(0.0)
            size_array.append(var_length)
            sort_size_array.append([var_length, id])
            id = id + 1
            
        if "[Trace]" in line and "free" in line:
            ptr_start = line.find("ptr") + len("ptr:")
            ptr_end = line.find("times:")
            time_start_loc = line.find("times:") + len("times:")
            
            time = (float(line[time_start_loc:]) - start_time) / (end_time - start_time)
            ptr = line[ptr_start:ptr_end]
            #print ptr
            #print ptr_array
            var_num = ptr_array.index(ptr)
            #print var_num
            end_times[var_num] = time
            #print len(end_times)
            ptr_array[var_num] = 0
            time_point.append(time - 0.000001)
            aggregate_size.append( float(total_size) / (1024 * 1024))
            alive_count_array.append(alive_count)
            
            total_size -= size_array[var_num]
            alive_count -= 1

            time_point.append(time)
            aggregate_size.append( float(total_size) / (1024 * 1024))
            alive_count_array.append(alive_count)
            #ptr_array.append(ptr)

        if "[End]" in line:
            end_times = [time if x == 0.0 else x for x in end_times]
            
        line = trace_file.readline()
    if figure_type == 1:
        cdf_plot(size_array, dirname, 10000)
        #print "here"
    if figure_type == 2:
        size_plot(time_point, aggregate_size, dirname)
    if figure_type == 3:
        count_plot(time_point, alive_count_array, dirname)
    if figure_type == 4:
        GetFirstLastAcccessTime(first_access_times, last_access_times, dirname)
        sort_size_array.sort()
        lifetime_plot(start_times, end_times, first_access_times, last_access_times, size_array, sort_size_array, dirname)
#main
init_color()

if resultdir == "":
    resultdir = os.getcwd()

for parent, dirnames, filename in os.walk(rootdir):
    dirnames.sort()
    for dirname in dirnames:
        get_start_end_time(dirname)
        GetDataWork(dirname)


#cdf_plot(data, f, 10000)

if figure_type == 1: 
    #print "Here"
    plt.xlabel("CDF")
    plt.xlabel("variable size(bytes)")
    plt.legend(bbox_to_anchor=(0.6, 0.4), loc=2, borderaxespad=0.0)
    plt.savefig(resultdir + "/cdf.eps", format="eps")
    plt.clf()
    
if figure_type == 2:
    plt.ylabel("Total object footprint size(MB)", fontsize = 17)
    plt.xlabel("Normalized execution time", fontsize = 17)
    plt.legend(bbox_to_anchor=(0.6, 0.4), loc='best', borderaxespad=0.0)
    plt.savefig(resultdir + "/size.eps", format="eps")

if figure_type == 3:
    plt.ylabel("variable counts")
    plt.xlabel("Normalized time")
    plt.legend(bbox_to_anchor=(0.3, 0.35), loc='best', borderaxespad=0.0)
    plt.savefig(resultdir + "/count.eps", format="eps")
    
