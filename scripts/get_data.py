# Analysis from 3 different inputs and put the summary into one folder

import sys
import os
import string
from optparse import OptionParser

spec_name = ["400.perlbench",
"401.bzip2",
"403.gcc",
"410.bwaves",
"416.gamess",
"429.mcf",
"433.milc",
"434.zeusmp",
"435.gromacs",
"436.cactusAD",
"437.leslie3d",
"444.namd",
"445.gobmk",
"447.dealII",
"450.soplex",
"453.povray",
"454.calculix",
"456.hmmer",
"458.sjeng",
"459.GemsFDTD",
"462.libquantum",
"464.h264ref",
"465.tonto",
"470.lbm",
"471.omnetpp",
"473.astar",
"481.wrf",
"482.sphinx3",
"483.xalancbmk"
]

list_program_name = []
list_metrics_name = []
metrics_name_flag = 0
list_all = []
major_column = []
total_variables = 0
metrics_name_flag = 0
record_clomum_flag = 0
spec_input = ["ref", "train", "test"]
parsec_input = ["native", "simlarge", "simmedium"]
#var_obj_name = ["1_1", "2_1", "3_1", "4_1", "4_2", "4_3", "5_1", "5_2", "5_3", "6_1", "6_2", "6_3"]
#var_obj_name = ["1_1", "1_2", "1_3", "1_4", "1_5", "1_6", "1_7", "1_8", "1_9", "1_10", "1_11", "1_12"]
#var_obj_name = ["1_1", "2_1", "2_2", "2_3", "2_4", "2_5", "2_6", "2_7", "2_8", "2_9", "2_10", "2_11"]
var_obj_name = ["1_1", "2_1", "3_1", "4_1", "4_2", "4_3", "5_1", "5_2", "5_3", "6_1", "6_2", "6_3"]

parser = OptionParser()

parser.add_option("-i", "--input", dest="inputdir",
                  help = "input directory")
parser.add_option("-o", "--output", dest="outputdir",
                  help = "output directory")
parser.add_option("-m", "--model", dest="model",
                  help = "model: SPEC, parsec", default = "SPEC")
parser.add_option("-t", "--type", dest="id_type",
                  help = "idtype: default, special", default = "default")
parser.add_option("-n", "--name", dest="task_name",
                  help = "the task name")

(options, args) = parser.parse_args()

#print task_name
def find_name(task_name):
    task_name = str(task_name)
    task_name = task_name[:task_name.find("-")]
    for i in range(0, len(spec_name)):
        if task_name in spec_name[i]:
            task_name = spec_name[i][spec_name[i].find(".")+1:]
            break
    return task_name

def find_column(dirname):
    try:
        file = open(options.inputdir + "/" + dirname + "/variable_out-time-new")
    except:
        file = open(options.inputdir + "/" + dirname + "/variable_out-time")
    
    index_array = []
    if record_clomum_flag == 0:
        line = file.readline()
        while line:
            #print line
            major_column.append(line)
            line = file.readline()
    else:
        line = file.readline()
        while line:
            try:
                index_array.append(major_column.index(line))
            except:
                index_array.append(-1)
            line = file.readline()

    return index_array
        

def work(dirname):
    print dirname
    global list_all
    global list_metrics_name
    global total_variables
    global metrics_name_flag
    global record_clomum_flag
    list_input_result = []
    total_lines = 0
    try:
        f = open(options.inputdir + "/" + dirname + "/var_time_info-new")
    except:
        f = open(options.inputdir + "/" + dirname + "/var_time_info")
    
    line = f.readline()

    index_array = find_column(dirname)
    #print dirname
    print index_array
    while line:
        #print line
        if metrics_name_flag == 0 :
            metrics_name = line.strip('\n')
            metrics_name = metrics_name.replace('/', '_')
            list_metrics_name.append(metrics_name)
        line = f.readline()
        
        list_data = []
        if record_clomum_flag == 0:
            while line:
                if line == "\n":
                    break
                total_lines = total_lines + 1
                #print line.split()[1]
                #print line
                if line.split()[1] == "-nan":
                    list_data.append("0")
                else:
                    try:
                        list_data.append(line.split()[1])
                    except:
                        #a = 1
                        print "No index"
                        #list_data.append(line.split()[0])

                line = f.readline()
        else:
            list_data = ["0"] * total_variables
            i = 0
            while line:
                if line == "\n":
                    break
                total_lines = total_lines + 1
            
                if line.split()[1] == "-nan":
                    list_data[index_array[i]] = "0"
                else:
                    try:
                        list_data[index_array[i]] = line.split()[1]
                    except:
                       # print line.split()[1]
                        #print index_array[i]
                        #print total_variables
                        #print list_data[index_array[i]]
                        print "No index"
                        #list_data[index_array[i]] = line.split()[0]
                    
                line = f.readline()
                i += 1

        if total_variables == 0:
            total_variables = total_lines

        list_input_result.append(list_data)
        #list_all.append(list_input_result)

        line = f.readline()
        #print line
    list_all.append(list_input_result)
    metrics_name_flag = 1
    record_clomum_flag = 1

def output():
    global list_all
    fallout = open(options.outputdir + "/" + "total.dat", "w")
    for i in range(0, len(list_metrics_name)):
        #CHANGME
        #print list_metrics_name[i]
        #fout = open(options.outputdir + "/" + list_metrics_name[i] + ".dat", "w")
        job_name = find_name(options.task_name)
        fout = open(options.outputdir + "/" + job_name + "_" + list_metrics_name[i] + ".dat", "w")
        fout.write("ID\t")
        fallout.write(list_metrics_name[i] + "\t")
        for j in range(0, len(list_program_name)):
            fout.write(list_program_name[j] + "\t")
            fallout.write(list_program_name[j] + "\t")
        fout.write("\n")
        fallout.write("\n")
        #print total_variables 

        for k in range(0, total_variables):
            if options.id_type == "default":
                fout.write(str(k+1) + "_1" + "\t")
                fallout.write(str(k+1) + "_1" + "\t")
            else:
                fout.write(var_obj_name[k] + "\t")
                fallout.write(var_obj_name[k] + "\t")
            for j in range(0, len(list_program_name)):
                temp_list_input_result = list_all[j]
                temp_list = temp_list_input_result[i]
                #print temp_list
                fout.write(temp_list[k] + "\t")
                fallout.write(temp_list[k] + "\t")
            fout.write("\n")
            fallout.write("\n")
        fallout.write("\n")


print options.model
for parent, dirnames, filenames in os.walk(options.inputdir):
    if options.model == "SPEC":
        for i in range(0, len(spec_input)):
            for dirname in dirnames:
                if dirname == "test":
                    continue
                if dirname == "morefile":
                    continue
                if dirname == "reference":
                    continue
                if dirname == "cspeedtest" or dirname == "seqtest":
                    continue
                if dirname == "refs":
                    continue
                if dirname == "tests":
                    continue
                if dirname == "fftwBuild-prefix":
                    continue
                if spec_input[i] in dirname:
                    list_program_name.append(spec_input[i])
                    work(dirname)
    if options.model == "parsec":
        for i in range(0, len(parsec_input)):
            for dirname in dirnames:
                if dirname == "test":
                    continue
                if parsec_input[i] in dirname:
                    list_program_name.append(parsec_input[i])
                    work(dirname)
    

print (list_metrics_name)
output()
