import numpy as np
import statsmodels.api as sm
import matplotlib.pyplot as plt
import matplotlib as mpl
import sys
import os
import struct

mpl.rcParams['xtick.labelsize'] = '19'
mpl.rcParams['ytick.labelsize'] = '19'


data = []
index = 0
color = []
def init_color ():
    color.append("black")
    color.append("green")
    color.append("grey")
    color.append("blue")
    color.append("#0000FF")
    color.append("#8A2BE2")
    color.append("#A52A2A")
    color.append("#5F9EA0")
    color.append("#7FFF00")
    color.append("#000000")

def CdfPlot(data, name, number, markers):
    global index
    #print 'data'
    #print data 
    ecdf = sm.distributions.ECDF(data)
    x = np.linspace(0, 1, 10)
    #    print x
    y = ecdf(x)

    #y = y * number
    plt.plot(x, y, label=name, marker = markers, markersize = 10, linewidth = 2) # color = color[index])
    index += 1



def ReadFile(file_name):
    f = open(file_name, "r")
    line = f.readline()
    while line:
        data.append(float(line))
        line = f.readline()

init_color()
        
data_dir = "/home/matrix/Work/deepmap/results/peak_memory_time/"

data = []
ReadFile(data_dir + "peak_80")
CdfPlot(data, "80% Peak FP", 15, ".")
#print data
data = []
ReadFile(data_dir + "peak_90")
CdfPlot(data, "90% Peak FP", 15, "o")

data = []
ReadFile(data_dir + "peak")
CdfPlot(data, "Peak FP", 15, "*")

#data = []
#ReadFile(data_dir + "peak_80_others")
#CdfPlot(data, "Other 80% Peak FP", 23, ".")
#
#data = []
#ReadFile(data_dir + "peak_90_others")
#CdfPlot(data, "Other 90% Peak FP", 23, "o")
#
#data = []
#ReadFile(data_dir + "peak_others")
#CdfPlot(data, "Other Peak FP", 23, "*")
#
plt.ylabel("Percentage(By app count)", fontsize = 22)
plt.xlabel("Normalized lifetime", fontsize = 22)

##plt.plot(0.2, 0, 0.2, 31, linewidth=2.0, color="000000")
#lines = plt.plot(0.2, 0, 0.2, 31)
# use keyword args
#plt.setp(lines, color='r', linewidth=2.0)
x = [0.2, 0.2]
y = [0, 1.05]
plt.plot(x, y, linewidth = 2.0, color = 'black')
plt.xlim([0.0, 1])
plt.ylim([0, 1.05])
plt.legend(bbox_to_anchor=(0.3, 0.4), loc=2, borderaxespad=0.0, fontsize = 18)
plt.savefig("/home/matrix/peak_fp_cdf.eps", format="eps")
plt.clf()
