#for analysis the the type behavior

import sys
import math
raw_file = open("./var_size_file", "r")

line = raw_file.readline()

obj_address = {}
obj_size = {}
obj_time = {}
var_sets = {}
obj_var = {}
total_obj = 0
cate_starttime = {}
cate_endtime = {}
cate_totaltime = {}
obj_flag = {}
last_time = 0.0
cate_count = {}
cate_livetime = {}
total_footprint = 0
max_total_footprint = 0
top10_varids = []
top10_cates = []
top10_consum = []
cate_totalsize = []

def ReadCate():
    file = open("./Cate", "r")
    line = file.readline()
    while line:
        cate = line.split()[1]
        if cate == "hashtables":
            cate_id = 0
        elif cate == "buffer":
            cate_id = 1
        elif cate == "vector":
            cate_id = 2
        elif cate == "bitmap":
            cate_id = 3
        elif cate == "index":
            cate_id = 4
        elif cate == "1D-other":
            cate_id = 5
        elif cate == "2d":
            cate_id = 6
        elif cate == "single":
            cate_id = 7
        elif cate == "2d-collection":
            cate_id = 8
        elif cate == "2d-matrix":
            cate_id = 6

        top10_varids.append(int(line.split()[0]))
        top10_cates.append(cate_id)
        #top10_consum.append(0)
        #cate_totalsize.append(0)
        line = file.readline()        
    for i in range(0, 9):
        cate_totalsize.append(0)

ReadCate()    

def FindInCate(var_id):
    global top10_varids
    for i in range(len(top10_varids)):
        if var_id == top10_varids[i]:
            return 1
    return 0

def FindCateID(var_id):
    global top10_varids
    global top10_cates
    for i in range(len(top10_varids)):
        if var_id == top10_varids[i]:
            return top10_cates[i]

def HandleAlloc(obj_id, var_id, ptr, size, time):
    global total_obj
    global total_footprint
    global max_total_footprint
                   
    if FindInCate(var_id) == 0:
        return

    cate_id = FindCateID(var_id)
    
    obj_address[ptr] = total_obj
    obj_size[total_obj] = size
    obj_var[total_obj] = var_id
    obj_time[total_obj] = time
    obj_flag[total_obj] = 1
                
    if cate_id not in cate_count:
        cate_count[cate_id] = 1
        cate_livetime[cate_id] = 0.0
        cate_totalsize[cate_id] += size
    else:
        cate_count[cate_id] += 1
        cate_totalsize[cate_id] += size
        
    if cate_count[cate_id] == 1:
        cate_starttime[cate_id] = time
        
    total_obj = total_obj + 1
    #print total_obj

def HandleFree(ptr, time):
    global total_footprint
    global max_total_footprint
    
    
    try:
        if obj_address[ptr] == -1:
            return
        var_id = obj_var[obj_address[ptr]]
        obj_id = obj_address[ptr]
    except:
        #print "What alloc or free?"
        return

    #var_footprint[var_id] -= obj_size[obj_address[ptr]]

    cate_id = FindCateID(var_id)

    if cate_id in cate_totaltime:
        cate_totaltime[cate_id] += \
                (time - obj_time[obj_address[ptr]]) * obj_size[obj_address[ptr]]
    else: 
        cate_totaltime[cate_id] = \
                (time - obj_time[obj_address[ptr]]) * obj_size[obj_address[ptr]]

    #print "aaaa", cate_id, cate_totaltime[cate_id], time
    
    cate_count[cate_id] -= 1

    if cate_count[cate_id] < 0:
        print "Warning!!", ptr, var_id, cate_id
    if cate_count[cate_id] == 0:
        cate_livetime[cate_id] += time - cate_starttime[cate_id]

    obj_flag[obj_address[ptr]] = 0
    obj_size[obj_address[ptr]] = 0
    obj_address[ptr] = -1

while line:
    if "malloc" in line:
        obj_id = int(line[line.find("malloc:") + 7:line.find("var_id")])
        var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
        ptr = line[line.find("ptr:") + 4:line.find("size")]
        size = int(line[line.find("size:") + 5:line.find("times")])
        time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
        HandleAlloc(obj_id, var_id, ptr, size, time)

    if "calloc" in line:
        obj_id = int(line[line.find("calloc:") + 7:line.find("var_id")])
        var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
        ptr = line[line.find("ptr:") + 4:line.find("size")]
        size = int(line[line.find("size:") + 5:line.find("times")])
        time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
        HandleAlloc(obj_id, var_id, ptr, size, time)

    if "realloc" in line:
        obj_id = int(line[line.find("realloc:") + 8:line.find("var_id")])
        var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
        ptr = line[line.find("ptr:") + 4:line.find("before")]
        size = int(line[line.find("size:") + 5:line.find("times")])
        pre_ptr = line[line.find("before:") + 7:line.find("size")]
        time = float(line[line.find("times:") + 6:].replace('\n', ''))

        HandleFree(pre_ptr, time)
        HandleAlloc(obj_id, var_id, ptr, size, time)

    if "free" in line:
        ptr = line[line.find("ptr:") + 4:line.find("times")]
        time = float(line[line.find("times:") + 6:].replace('\n', ''))

        HandleFree(ptr, time)

    if "times:" in line:
        last_time = float(line[line.find("times:") + 6:].replace('\n', ''))

    line = raw_file.readline()

#deal with the unfree malloc

for i in range(0, len(obj_flag)):
    if obj_flag[i] == 1:
        var_id = obj_var[i]
        
        if FindInCate(var_id) == 0:
            continue
        
        cate_id = FindCateID(var_id)
        cate_count[cate_id] -= 1

        #print "here"
        if cate_count[cate_id] == 0:
            cate_livetime[cate_id] += last_time - cate_starttime[cate_id]

        if cate_id in cate_totaltime:
            cate_totaltime[cate_id] += \
                (last_time - obj_time[i]) * obj_size[i]
        else: 
            cate_totaltime[cate_id] = \
                (last_time - obj_time[i]) * obj_size[i]

#print "Var_ID", "#_of_objects", "Avg", "Footprint", "Varivance_%", "Avg_#_of_concurrent"


for i in range(0, 9):
    if i not in cate_totaltime:
        #cate_totalsize[i] = 0
        cate_totaltime[i] = 0
        cate_livetime[i] = 1

cate_avg_consum = []
for i in range(0, 9):
    cate_avg_consum.append(cate_totaltime[i] / cate_livetime[i])
    #print cate_totaltime[i], cate_livetime[i]

print top10_cates.count(0), top10_cates.count(1), \
    top10_cates.count(2), top10_cates.count(3), top10_cates.count(4), \
    top10_cates.count(5), top10_cates.count(6), top10_cates.count(7), top10_cates.count(8)

print cate_totalsize[0], cate_totalsize[1], \
    cate_totalsize[2], cate_totalsize[3], cate_totalsize[4], \
    cate_totalsize[5], cate_totalsize[6], cate_totalsize[7], cate_totalsize[8]
    
print cate_avg_consum[0], cate_avg_consum[1], \
    cate_avg_consum[2], cate_avg_consum[3], cate_avg_consum[4], \
    cate_avg_consum[5], cate_avg_consum[6], cate_avg_consum[7], cate_avg_consum[8]
