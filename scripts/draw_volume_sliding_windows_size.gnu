set term eps
set output "volume_sliding_window.eps"
set style data histogram
set style histogram clustered errorbars gap 1 lw 3
#set style fill solid 1

set ytics font ",22"
set xtics font ",24"
set ylabel "CV" font ",24" offset -1
set xlabel "Episode length (billion instructions)" font ",24"
set key right font ",24"
set yrange[0:3.5]
set ytics 0,1,3
set label 1 "5.58" font ",24" at 0.2, 3.2

set lmargin at screen 0.18
set bmargin at screen 0.2

plot "volume_sliding_window_size.dat" using 3:4:5:xticlabels(2) title columnheader(3)  linecolor rgb "#000000" , \
'' using 6:7:8:xticlabels(2) title columnheader(6) linecolor "blue"

#plot "volume_sliding_window_size.dat" using 1:3:4:5:xticlabels(2) with yerrorbars title columnheader(2) linecolor rgb "#000000" , \
#'' using 1:6:7:8:xticlabels(2) with yerrorbars title columnheader(6) linecolor rgb "blue", \


#'' using 2 title columnheader(2) w lines linecolor rgb "grey" lw 2, \
#'' using 1:6 title columnheader(6) w lines linecolor rgb "#0000FF" lw 2 pt 7 ps 0.5, \
#'' using 1:7 title columnheader(7) w lp linecolor rgb "#FF0000" lw 2 pt 5 ps 0.5
