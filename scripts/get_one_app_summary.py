#get lifetime information and get the physical memory and footprint memory information figures
#usage: see -h

import sys
import math
#import statsmodels.api as sm
import matplotlib.pyplot as plt
import matplotlib as mpl
from optparse import OptionParser
import os
import time

mpl.rcParams['xtick.labelsize'] = '16'
mpl.rcParams['ytick.labelsize'] = '16'

color = []
footprint_points = []
time_points = []
footprint = 0
color_count = 0
spec_input = ["ref", "train", "run_test"]
parsec_input = ["native", "simlarge", "test"]
obj_address = {}
obj_size = {}
total_obj = 0
start_time = 0.0
end_time = 0.0
total_footprint = 0
peak_time = 0.0
peak_90_time = 0.0
peak_80_time = 0.0
peak_100_time = 0.0
time_20_memory = 0.0
time_50_memory = 0.0
max_var_id = 0

max_footprint = 0
alive_var = {}
alive_obj = 0
max_obj = 0 # max alive object

spec_name = ["400.perlbench",
"401.bzip2",
"403.gcc",
"410.bwaves",
"416.gamess",
"429.mcf",
"433.milc",
"434.zeusmp",
"435.gromacs",
"436.cactusAD",
"437.leslie3d",
"444.namd",
"445.gobmk",
"447.dealII",
"450.soplex",
"453.povray",
"454.calculix",
"456.hmmer",
"458.sjeng",
"459.GemsFDTD",
"462.libquantum",
"464.h264ref",
"465.tonto",
"470.lbm",
"471.omnetpp",
"473.astar",
"481.wrf",
"482.sphinx3",
"483.xalancbmk"
]

parser = OptionParser()

parser.add_option("-i", "--input", dest="inputdir",
                  help = "input directory", default = "")
parser.add_option("-o", "--output", dest="outputdir",
                  help = "output directory")
parser.add_option("-m", "--model", dest="model",
                  help = "model: SPEC, parsec", default = "SPEC")
parser.add_option("-n", "--name", dest="task_name",
                  help = "the task name")
parser.add_option("-t", "--reduce", dest="reduce_times",
                  help = "the task name", default = "1")


(options, args) = parser.parse_args()

def init_color ():
    color.append("#0000FF")
    color.append("#FF0000")
    color.append("#000000")
    color.append("#FF00FF")
    color.append("#00FF00")
    color.append("#000000")

init_color()

def find_name(task_name):
    task_name = str(task_name)
    task_name = task_name[:task_name.find("-")]
    for i in range(0, len(spec_name)):
        if task_name in spec_name[i]:
            task_name = spec_name[i][spec_name[i].find(".")+1:]
            break
    return task_name

def size_plot(time_data, size_data, label_name):
    global color_count
    plt.plot(time_data, size_data, '-',  color=color[color_count], markersize=4, label=label_name)
    color_count = color_count + 1

def HandleAlloc(obj_id, var_id, ptr, size, time):
    global footprint
    global total_obj
    global footprint_points
    global time_points
    global total_footprint
    #global peak_100_time
    #global peak_90_time
    #global peak_80_time
    global time_50_memory
    global time_20_memory
    global max_var_id
    
    
    global max_footprint
    global alive_obj 
    global max_obj

    obj_address[ptr] = total_obj
    obj_size[total_obj] = size
        
    time_points.append(time)
    footprint_points.append(float(footprint) / (1024 * 1024))
    footprint += size
    time_points.append(time)
    footprint_points.append(float(footprint) / (1024 * 1024))

    if max_footprint < footprint:
        max_footprint = footprint
        peak_100_time = time

    alive_obj += 1
    if max_obj < alive_obj:
        max_obj = alive_obj

    if max_var_id < var_id:
        max_var_id = var_id
    if (time - start_time) / (end_time - start_time) < 0.2:
        alive_var[var_id] = 1

    #if total_footprint != 0:
    #    if (time - start_time) / (end_time - start_time) < 0.2 and time_20_memory < footprint:
    #        time_20_memory = footprint;
    #    if (time - start_time) / (end_time - start_time) < 0.5 and time_50_memory < footprint:
    #        time_50_memory = footprint;

    #if total_footprint != 0:
    #    if footprint >= total_footprint * 0.9 and peak_90_time == 0.0:
    #        peak_90_time = time
    #    if footprint >= total_footprint * 0.8 and peak_80_time == 0.0:
    #        peak_80_time = time

    total_obj = total_obj + 1
    
def HandleFree(ptr, time):
    global footprint
    global footprint_points
    global time_points
    global alive_obj
    #global time_50_memory
    #global time_20_memory
    alive_obj -= 1

    try:
        
        time_points.append(time)
        footprint_points.append(float(footprint) / (1024 * 1024))
        
        footprint -= obj_size[obj_address[ptr]]
        obj_size[obj_address[ptr]] = 0
        
        time_points.append(time)
        footprint_points.append(float(footprint) / (1024 * 1024))
    except:
        a = 1
        #print "What?!"
        
def work(dirname, input_name):
    global start_time
    global end_time
    global footprint_points
    global time_points
    global footprint
   
    raw_file = open(options.inputdir + "/" + dirname + "/var_size_file", "r")
    time_points = []
    footprint_points = []
    footprint = 0

    line = raw_file.readline()

    while line:
        
        if "malloc" in line:
            obj_id = int(line[line.find("malloc:") + 7:line.find("var_id")])
            var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
            ptr = line[line.find("ptr:") + 4:line.find("size")]
            size = int(line[line.find("size:") + 5:line.find("times")])
            time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
            HandleAlloc(obj_id, var_id, ptr, size, time)

        if "calloc" in line:
            obj_id = int(line[line.find("calloc:") + 7:line.find("var_id")])
            var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
            ptr = line[line.find("ptr:") + 4:line.find("size")]
            size = int(line[line.find("size:") + 5:line.find("times")])
            time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
            HandleAlloc(obj_id, var_id, ptr, size, time)

        if "realloc" in line:
            obj_id = int(line[line.find("realloc:") + 8:line.find("var_id")])
            var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
            ptr = line[line.find("ptr:") + 4:line.find("before")]
            size = int(line[line.find("size:") + 5:line.find("times")])
            pre_ptr = line[line.find("before:") + 7:line.find("size")]
            time = float(line[line.find("times:") + 6:].replace('\n', ''))
            
            HandleFree(pre_ptr, time)
            HandleAlloc(obj_id, var_id, ptr, size, time)

        if "free" in line:
            ptr = line[line.find("ptr:") + 4:line.find("times")]
            time = float(line[line.find("times:") + 6:].replace('\n', ''))

            HandleFree(ptr, time)

        if "times:" in line:
            last_time = float(line[line.find("times:") + 6:].replace('\n', ''))

        if "Start" in line:
            start_time = float(line[line.find("time:") + 5:].replace('\n', ''))

        if "End" in line:
            end_time = float(line[line.find("time:") + 5:].replace('\n', ''))
            
        line = raw_file.readline()
        

    print input_name
    for i in range(0, len(time_points)):
        time_points[i] = (time_points[i] - start_time) / (end_time - start_time)

    time_points_reduce = []
    footprint_points_reduce = []
    flag = 0
    p = 0.0
    for i in range(0, len(time_points)):
        if i % int(options.reduce_times) == 0:
            time_points_reduce.append(time_points[i])
            footprint_points_reduce.append(footprint_points[i])
            #time_points_reduce.append(time_points[i+1])
            #footprint_points_reduce.append(footprint_points[i+1])
    #print footprint_points
    #print time_points
    #size_plot(time_points, footprint_points, input_name)
    #size_plot(time_points_reduce, footprint_points_reduce, input_name)
    print input_name 
    for i in range(0, len(time_points_reduce)):
        if time_points_reduce[i] / 2 > float(p / 100.0):
            print time_points_reduce[i] / 2, footprint_points_reduce[i]
            p += 1
        #print time_points_reduce[i], footprint_points_reduce[i]

#for geting the physical memory consumption
'''
    raw_file = open(options.inputdir + "/" + dirname + "/mem_consumption", "r")

    line = raw_file.readline()
    total_line = 0
    pm_time = []
    pm_data = []

    while line:
        pm_data.append(float(line.split()[1]) / 1024)
        total_line = total_line + 1
        line = raw_file.readline()
    
    for i in range(0, total_line):
        pm_time.append(float(i) / total_line)
        

    size_plot(pm_time, pm_data, "PM")
'''

work("home/xji/softwares/euler3g_ilu_original/run_big", "")
total_footprint = max_footprint
footprint = 0
obj_address = {}
obj_size = {}
peak_time = peak_100_time
alive_obj = 0
total_obj = 0
print max_footprint
print max_obj
#work("mt/digitrecognition/run_ref", "")
#print max_footprint
#print max_obj


'''
#print "peak_time:\t", (peak_time - start_time) / (end_time - start_time)
#print "peak_90_time:\t", (peak_90_time - start_time) / (end_time - start_time)
#print "peak_80_time:\t", (peak_80_time - start_time) / (end_time - start_time)
#print "time_20%_memory", time_20_memory / float(total_footprint)
#print "time_50%_memory", time_50_memory / float(total_footprint)

'''
'''
last_line = os.popen("tail -n 1 ./var_size_file > ./tmp_end_time")
time.sleep(1)
f = open("./tmp_end_time", "r")
line = f.readline()
if "End" in line:
    end_time = float(line[line.find("time:") + 5:].replace('\n', ''))
work(".", "")
print len(alive_var)
'''
