import sys
from multiprocessing import Pool, Queue, Manager
import os, time, random
from optparse import OptionParser

parameter_file = "Parameter"
jobs_file = "Jobs"

task_command = []
task_name = []
num_cores = 20

parser = OptionParser()

parser.add_option("-p", "--parameter", dest="parameter_file",
                  help = "parameter", default = "Parameter")
parser.add_option("-j", "--jobs", dest="job_file",
                  help = "jobs", default = "Jobs")
parser.add_option("-t", "--tasks", dest="task_num",
                  help = "task num")

(options, args) = parser.parse_args()

spec_name = ["400.perlbench",
"401.bzip2",
"403.gcc",
"410.bwaves",
"416.gamess",
"429.mcf",
"433.milc",
"434.zeusmp",
"435.gromacs",
"436.cactusAD",
"437.leslie3d",
"444.namd",
"445.gobmk",
"447.dealII",
"450.soplex",
"453.povray",
"454.calculix",
"456.hmmer",
"458.sjeng",
"459.GemsFDTD",
"462.libquantum",
"464.h264", # NOTICE here
"465.tonto",
"470.lbm",
"471.omnetpp",
"473.astar",
"481.wrf",
"482.sphinx3",
"483.xalancbmk"
]

def find_name(task_name):
    task_name = str(task_name)
    task_name = task_name[:task_name.find("-")]
    for i in range(0, len(spec_name)):
        if task_name in spec_name[i]:
            task_name = spec_name[i][spec_name[i].find(".")+1:]
            break
    return task_name

def GetParameter(parameter_file):
    f = open(parameter_file)
    line = f.readline()
    directory = ""
    command = ""
    name = ""
    job_name = ""
    while line:
        if "[Name]" in line:
            name = line[line.find("[Name]") + 6 :].replace("\n","")
            task_name.append(name)
            job_name = find_name(name)

        if "[Dir]" in line:
            directory = line[line.find("[Dir]") + 5 :].replace("\n","")

        if "[Command]" in line:
            command = line[line.find("[Command]") + 9 :].replace("\n","")
            exe_name = command.split()[0]
            #print exe_name
            #print command
            #task_command.append("cd " + directory + "; precalltrack " + command + " > ./time_res 2>&1")`
            #task_command.append("cd " + directory + "; preinfo " + command + " > ./heap_stack_ratio_pc 2>&1")
            #task_command.append("cd " + directory + "; cat inscount.out >> ~/code_volume")
            #task_command.append("cd " + directory + "; echo " + job_name + " >> ~/total_volume; cat heap_volume_res >> ~/total_volume")
            #task_command.append("cd " + directory + "; echo " + job_name + " >> ~/component_volume; cat category_volume_res >> ~/component_volume")
            #task_command.append("cd " + directory + "; echo " + job_name + " >> ~/cate_volume_result; grep \"Stack address is 0\" res_cate_volume >> ~/cate_volume_result")
            
            #task_command.append("cd " + directory + "; size " + exe_name + " | tail -n 1 | awk '{print $3}' >> ~/bss")
            #task_command.append("cd " + directory + "; /home/xji/pin-2.14-71313-gcc.4.4.7-linux/intel64/bin/pinbin -t /home/xji/deepmap/pin-tools/obj-intel64/volume.so -- " + command + " > ./code_volume 2>&1")
            task_command.append("cd " + directory + "; /home/xji/pin-2.14-71313-gcc.4.4.7-linux/intel64/bin/pinbin -t /home/xji/deepmap/pin-tools/obj-intel64/datatool.so -- " + command + " > ./res_cate_volume 2>&1")
            
            #task_command.append("cd " + directory + "; time " + command + " > ./solo_time_res 2>&1")
            #task_command.append("cd " + directory + "; tail -n 2 solo_time_res | head -n 1 >> ~/solo_time_result 2>&1")
            #task_command.append("cd " + directory + "; tail -n 3 pinres | head -n 1 >> ~/pin_result 2>&1")
            #task_command.append("cd " + directory + "; memusg " + command + " > ./mem_res")
            #task_command.append("cd " + directory + "; cat solo_time >> ~/deepmap/scripts/rate/" + name[:name.find("-")] +"_solo_time 2>&1")
            #task_command.append("cd " + directory + "; /usr/bin/time -f \"%e\" " + command + " 2> ./solo_time")
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_cov.py > ./var_result 2>&1")
            #task_command.append("cd " + directory + "; cat var_result | grep -v What | sort -rn -k4 | awk '{print $1}' | head -n 10 > VarIDs")
            #task_command.append("cd " + directory + "; cat var_result | sort -rn -k4 | head -n 10 > VarRes")
            #task_command.append("cd " + directory + "; echo " + name[0:3] + "1" + " > ./QueueKey ")
            #task_command.append("cd " + directory + "; cp ./VarMinSize.3 VarMinSize")
            #task_command.append("cd " + directory + "; cp ./VarMinSize.1 VarMinSize")
            #task_command.append("cd " + directory + "; cp TraceSize TraceSize.bak; cp VarMinSize.1 VarMinSize; cat ./VarMinSize | awk '{if ($1 > 0) print $1 }' | sort -n | head -n 1 > TraceSize")
            
            #task_command.append("cd " + directory + "; cp TraceSize TraceSize.1; cat ./VarMinSize | awk '{if ($1 > 0) print $1; if($2 > 0) print $2; if ($3 > 0) print $3; }' | sort -n | head -n 1 > TraceSize")
            #task_command.append("cd " + directory + "; /home/xji/pin-2.14-71313-gcc.4.4.7-linux/intel64/bin/pinbin -t /home/xji/deepmap/pin-tools/obj-intel64/pintool.so -- " + command + " > ./res 2>&1")
            #task_command.append("cd " + directory + "; precalltrack /home/xji/pin-2.14-71313-gcc.4.4.7-linux/intel64/bin/pinbin -t /home/xji/deepmap/pin-tools/obj-intel64/pintool.so -- " + command + " > ./pinres_new 2>&1")
            #task_command.append("cd " + directory + "; /home/xji/pin-2.14-71313-gcc.4.4.7-linux/intel64/bin/pinbin -t /home/xji/deepmap/pin-tools/obj-intel64/heaptool.so -- " + command + " > ./heap_res 2>&1")
            #task_command.append("cd " + directory + "; cat top10-calltrack-newest | grep Hash | awk '{print $2}' | sort | uniq > ../HashValues")
            #task_command.append("cd " + directory + "; cat top10-calltrack | grep Hash | awk '{print $2}' | sort | wc -l >> ~/test1")
            #task_command.append("cd " + directory + "; cp ../HashValues .")
            #task_command.append("cd " + directory + "; cp ./var_size_file ./var_size_file_bakup")
            #task_command.append("cd " + directory + "; echo " + name + " >> ~/res; cat TraceSize >> ~/res")
            #task_command.append("cd " + directory + "; cat QueueKey >> ~/res")
            #task_command.append("cd " + directory + "; cat footprint | grep object | awk \'{print $6}\' > ./TraceSize")
            #task_command.append("cd ~/deepmap/scripts; python get_lifetime.py -i " + directory + "/.. -o ./lifetime -n " + name)
            #task_command.append("python get_data.py -i " + directory + "/.. -o ~/time_good_res/ -n " + name + " > ./result/" + name +".res" )
            #task_command.append("cd " + directory + "; sort -n VarIDs > sorted_VarIDs; python ~/deepmap/scripts/get_lifetime1.py -i " + directory + " -o ~/deepmap/scripts/sizes_lifetimes -n " + name)
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_lifetime1.py -i " + directory + " -o ../" + job_name + " -n " + name)
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_lifetime1.py -i " + directory + " -o ~/deepmap/scripts/lifetimes -n " + name)
            #task_command.append("cd " + directory + "; sort -n VarIDs > Cate")
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_consumption.py > type_result")
            #task_command.append("cd " + directory + "; head -n 2 Cate_result  | tail -n 1 >> ~/Cates_size")
            #task_command.append("cd " + directory + "; head -n 2 scaling_factor  | tail -n 1 >> ~/scaling_factor_mid")
            #task_command.append("cd " + directory + "; tail -n 1 mem_res >> ~/mem_res")
            #task_command.append("cd " + directory + "; cat res >> ~/res")
            #task_command.append("cd " + directory + "; cp top10-malloctrace.out-obj top10-malloctrace.out")
            #task_command.append("cd " + directory + ";  python ~/deepmap/scripts/analysis_type_scaling.py ../" + job_name + "/Size.dat >> ~/scaling_factor")
            #task_command.append("cd " + directory + "; echo End >> scaling_factor; echo End >> scaling_factor")
            #task_command.append("cd " + directory + "; cat type_result  | tail -n 1 >> ~/type_footprint")
            #task_command.append("cd " + directory + "; echo " + job_name + " >> ~/all_cate; awk '{print $2}' Cate >> ~/all_cate;")
            #task_command.append("cd " + directory + "; cd ../" + job_name + "; awk 'NR==FNR {a[NR]=$2} NR>FNR {if(FNR>1) print a[FNR], $1}' access_bytes_size.dat *_single_lifetime > ~/density_lifetimes/" + job_name + "_density_lifetime")
            #task_command.append("cd " + directory + "; cp test_output ~/calltracks/" + job_name + "_calltrack;")
            #task_command.append("cd " + directory + "; cp Cate ~/category/" + job_name + "_cate;")
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_lifetime.py -i . > time_20_alive_vars") #peak_consumpation_time_result time_memory_result
            #task_command.append("cd " + directory + ";  head -n 1 time_memory_result | tail -n 1 | awk '{print $2}' >> ~/time_20_memory")
            #task_command.append("cd " + directory + "; wc -l variable_out-time >> ~/restime")
            #task_command.append("cd " + directory + "; cp var_result var_result.old; cp var_result1 var_result")
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_sd.py " + job_name)
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_sd.py " + job_name)
            #task_command.append("cd " + directory + "; cat time_20_alive_vars | tail -n 1 >> ~/time_20_alive_vars")
            #task_command.append("cd " + directory + "; tail -n 3 var_size_file | head -n 1 | awk '{print $5}' >> ~/total_vars")
            #task_command.append("cd " + directory + "; cp ~/deepmap/results/final_category/" + job_name + "_Cate Cate;")
            #task_command.append("wc -l " + directory + "/variable_out_obj >> ~/obj_res")
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_info_obj.py " + directory + " > ./summary_per_obj")
            #task_command.append("cd " + directory + "; tail -n 1 ./summary_per_obj >> ~/summary_pre_obj")
            #task_command.append("cd " + directory + "; cp summary_per_obj ~/objs_result/" + job_name)
            #task_command.append("cd " + directory + "; cp Cate Cate_all")
            #task_command.append("cd " + directory + "; cp stack_res ~/stacks/" + job_name)
            #task_command.append("cd " + directory + "; python ~/deepmap/scripts/get_sd.py")
        line = f.readline()

def GetJobs(jobs_file, task_sets):
    #f = open(jobs_file)
    f = open(jobs_file)
    index = 0
    line = f.readline()
    while line: 
        items = line.split()
    
        if items[0] in task_name:
            for j in range(int(items[1])):
        
                if index > task_num:
                    print "Error: Too many jobs"
                    exit
                task_id = task_name.index(items[0])
                task_sets[index] = task_id
                index = index + 1
                if index == len(task_sets):
                    break
        else:
            print "Jobs name not in the list!"
        if index == len(task_sets):
            break
        line = f.readline()

def RunFunc(id, task_id, task_command):
    os.popen(task_command[task_id])
    print id, task_command[task_id]

if __name__=='__main__':
    
    p = Pool(num_cores)
    manager = Manager()
    task_num = int(options.task_num)
    
    #task_sets = manager.Array('i', range(task_num))
    task_sets = [0] * task_num
    
    GetParameter(options.parameter_file)
    GetJobs(options.job_file, task_sets)
    print task_sets

    for i in range(0, task_num):
        p.apply_async(RunFunc, args=(i, task_sets[i], task_command))
        time.sleep(1)

    p.close()
    p.join()
