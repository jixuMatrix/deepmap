set term eps 
set output outputfile
set style data histogram
set style fill solid 1 border 0
#set style histogram clustered gap 0.5
set style histogram rowstacked

#set style fill pattern 0  border 1
#set title "Varible size of bzip2"
set format y "%g%%"
#set key font ",20" width 4.5 #height 1
set key font ",16" # width 0.0 spacing 1.2 
#set yrange [0:115]
set yrange [0:105]
#set key top
#set key outside above Left width 1
#set key right 
#set key at 10, 10
set key horizontal samplen 2
#set key samplen 1
#set key noreverse

#set style histogram rowstacked
set boxwidth 0.5 relative
set xtics font ",16"   
set ytics font ",16"
set bmargin at screen 0.18 
set lmargin at screen 0.12
set xtics rotate by -45
#filename = "seq_ratio.dat"
plot inputfile using 2:xticlabels(1) title columnheader(2) linecolor rgb "#000000", \
'' using 3:xticlabels(1) title columnheader(3) linecolor rgb "#FFFFFF", \
'' using 4:xticlabels(1) title columnheader(4) linecolor rgb "#BEBEBE", \
'' using 5:xticlabels(1) title columnheader(5) linecolor rgb "#000000" fillstyle pattern 2, \
'' using 6:xticlabels(1) title columnheader(6) linecolor rgb "0000000" fillstyle pattern 7;
##"000000" fillstyle pattern 1, \
