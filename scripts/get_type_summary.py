import sys
import math

CHANGE_RATIO = 10

cate_array = []
vars_missing = []
vars_large = []
vars_scaling = []
vars_fixed = []
vars_irrgular = []
vars_read_big = []
vars_read_mid = []
vars_read_small = []
vars_read_change = []
vars_seq_big = []
vars_seq_mid = []
vars_seq_small = []
vars_seq_change = []
vars_stride_big = []
vars_stride_mid = []
vars_stride_small = []
vars_stride_change = []
vars_density_fixed = []
vars_density_irrgular = []
vars_density_scaling = []
vars_tmp_page_change = []
vars_tmp_cache_change = []
vars_spa_cache_change = []
vars_spa_page_change = []
app_change_count = 0
app_irrgular_count = 0
app_fixed_count = 0
app_scaling_count = 0
vars_density_cate = []
vars_read_cate = []
vars_stride_cate = []
vars_seq_cate = []
vars_tmp_loc = []
vars_spa_loc = []
vars_density_big = []


def GetCate(app_name):
    file = open(app_name + "/Final_Cate", "r")
    line = file.readline()
    while line:
        cate = line.split()[1]
        if cate == "hashtables":
            cate_id = 1
        elif cate == "buffer":
            cate_id = 2
        elif cate == "vector":
            cate_id = 3
        elif cate == "bitmap":
            cate_id = 5
        elif cate == "index":
            cate_id = 4
        elif cate == "1D-other":
            cate_id = 3
        elif cate == "2d":
            cate_id = 6
        elif cate == "single":
            cate_id = 0
        elif cate == "2d-collection":
            cate_id = 7
        elif cate == "2d-matrix":
            cate_id = 6
        elif cate == "MD":
            cate_id = 8

        cate_array.append(cate_id)
        line = file.readline()

def GetSize(app_name):
    size_file = open(app_name + "/Size.dat", "r")
    line = size_file.readline()
    line = size_file.readline()
 
    while line:
        big_size = int(line.split()[1])
        mid_size = int(line.split()[2])
        small_size = int(line.split()[3])
        var_missing = 0
        var_large = 1
        var_fixed = 0
        var_scaling = 0
        var_irrgular = 0
        
        if big_size == 0 or mid_size == 0 or small_size == 0:
            var_missing = 1
            
        elif big_size <= 4096:
            var_large = 0

        # Add temp
        elif big_size == mid_size and mid_size == small_size:
            var_fixed = 1
        elif big_size >= mid_size and mid_size >= small_size:
            var_scaling = 1
        else:
            var_irrgular = 1

        #if var_irrgular == 1:
            #print "irrgular!!!"
            #print big_size, mid_size, small_size
        vars_missing.append(var_missing)
        vars_large.append(var_large)
        vars_fixed.append(var_fixed)
        vars_scaling.append(var_scaling)
        vars_irrgular.append(var_irrgular)
        line = size_file.readline()

def GetDensityRatio(app_name):
    density_file = open(app_name + "/access_bytes_size.dat", "r")
    line = density_file.readline()
    line = density_file.readline()

    while line:
        var_density_fixed = 0
        var_density_scaling = 0
        var_density_irrgular = 0
        cate = 0
        big_density = float(line.split()[1])
        mid_density = float(line.split()[2])
        small_density = float(line.split()[3])
        #print line
        if abs(big_density - mid_density) <= CHANGE_RATIO * max(big_density, mid_density, small_density) / 100 and \
        abs(mid_density - small_density) <= CHANGE_RATIO * max(big_density, mid_density, small_density) / 100 and \
        abs(big_density - small_density) <= CHANGE_RATIO * max(big_density, mid_density, small_density) / 100:
            var_density_fixed = 1
        elif big_density > mid_density and mid_density > small_density:
            var_density_scaling = 1 
        elif big_density > mid_density and (mid_density - small_density) <= CHANGE_RATIO * max(big_density, mid_density, small_density) / 100:
            var_density_scaling = 1 
        elif mid_density and abs(big_density - small_density) <= CHANGE_RATIO * max(big_density, mid_density, small_density) / 100:
            var_density_scaling = 1
        else:
            var_density_irrgular = 1
        #print var_density_fixed, var_density_scaling, var_density_irrgular
        vars_density_fixed.append(var_density_fixed)
        vars_density_scaling.append(var_density_scaling)
        vars_density_irrgular.append(var_density_irrgular)

        if big_density < 0.1:
            cate = 1
        elif big_density < 1:
            cate = 2
        elif big_density < 100:
            cate = 3
        elif big_density < 10000:
            cate = 4
        else:
            cate = 5
        
        vars_density_big.append(big_density)
        vars_density_cate.append(cate)
        line = density_file.readline()

def GetTmpCacheRatio(app_name):
    tmp_cache_file = open(app_name + "/Temporal_locality_cache.dat", "r")
    line = tmp_cache_file.readline()
    line = tmp_cache_file.readline()

    while line:
        var_tmp_cache_change = 0
        cate = 0
        big_tmp_cache = float(line.split()[1])
        mid_tmp_cache = float(line.split()[2])
        small_tmp_cache = float(line.split()[3])
        if abs(big_tmp_cache - mid_tmp_cache) > CHANGE_RATIO or \
        abs(mid_tmp_cache - small_tmp_cache) > CHANGE_RATIO or \
        abs(big_tmp_cache - small_tmp_cache) > CHANGE_RATIO:
            var_tmp_cache_change = 1
        #print line
        if big_tmp_cache > 75:
            cate = 1
        elif big_tmp_cache < 25:
            cate = 2
        vars_tmp_loc.append(cate)
        vars_tmp_cache_change.append(var_tmp_cache_change)
                
        line = tmp_cache_file.readline()

def GetTmpPageRatio(app_name):
    tmp_page_file = open(app_name + "/Temporal_locality_page.dat", "r")
    line = tmp_page_file.readline()
    line = tmp_page_file.readline()

    while line:
        var_tmp_page_change = 0
        big_tmp_page = float(line.split()[1])
        mid_tmp_page = float(line.split()[2])
        small_tmp_page = float(line.split()[3])
        if abs(big_tmp_page - mid_tmp_page) > CHANGE_RATIO or \
        abs(mid_tmp_page - small_tmp_page) > CHANGE_RATIO or \
        abs(big_tmp_page - small_tmp_page) > CHANGE_RATIO:
            var_tmp_page_change = 1
        vars_tmp_page_change.append(var_tmp_page_change)
                
        line = tmp_page_file.readline()

def GetSpaCacheRatio(app_name):
    spa_cache_file = open(app_name + "/Spatial_locality_cache.dat", "r")
    line = spa_cache_file.readline()
    line = spa_cache_file.readline()

    while line:
        cate = 0
        var_spa_cache_change = 0
        big_spa_cache = float(line.split()[1])
        mid_spa_cache = float(line.split()[2])
        small_spa_cache = float(line.split()[3])
        if abs(big_spa_cache - mid_spa_cache) > CHANGE_RATIO or \
        abs(mid_spa_cache - small_spa_cache) > CHANGE_RATIO or \
        abs(big_spa_cache - small_spa_cache) > CHANGE_RATIO:
            var_spa_cache_change = 1
        vars_spa_cache_change.append(var_spa_cache_change)
        #print line
        if big_spa_cache > 75:
            cate = 1
        elif big_spa_cache < 25:
            cate = 2
        vars_spa_loc.append(cate)
        
        
        line = spa_cache_file.readline()

def GetSpaPageRatio(app_name):
    spa_page_file = open(app_name + "/Spatial_locality_page.dat", "r")
    line = spa_page_file.readline()
    line = spa_page_file.readline()

    while line:
        var_spa_page_change = 0
        big_spa_page = float(line.split()[1])
        mid_spa_page = float(line.split()[2])
        small_spa_page = float(line.split()[3])
        if abs(big_spa_page - mid_spa_page) > CHANGE_RATIO or \
        abs(mid_spa_page - small_spa_page) > CHANGE_RATIO or \
        abs(big_spa_page - small_spa_page) > CHANGE_RATIO:
            var_spa_page_change = 1
        vars_spa_page_change.append(var_spa_page_change)

                
        line = spa_page_file.readline()


def GetReadRatio(app_name):
    global app_change_count 
    global app_fixed_count 
    read_file = open(app_name + "/read_ratio_volume.dat", "r")
    line = read_file.readline()
    line = read_file.readline()

    app_change_count = 0
    app_fixed_count = 0
    while line:
        var_read_change = 0
        cate = 0
        big_read = float(line.split()[1])
        mid_read = float(line.split()[2])
        small_read = float(line.split()[3])
        if abs(big_read - mid_read) > CHANGE_RATIO or \
        abs(mid_read - small_read) > CHANGE_RATIO or \
        abs(big_read - small_read) > CHANGE_RATIO:
            var_read_change = 1
            app_change_count += 1
        else:
            app_fixed_count += 1
        vars_read_big.append(big_read)
        vars_read_mid.append(mid_read)
        vars_read_small.append(small_read)
        vars_read_change.append(var_read_change)
        #print line
        if big_read > 75:
            cate = 1
        elif big_read < 25:
            cate = 2
        else:
            cate = 3
        vars_read_cate.append(cate)
        #vars_random_big.append(100.0 - big_read)

        line = read_file.readline()

    #print app_name, app_fixed_count, app_change_count

def GetSeqRatio(app_name):
    seq_file = open(app_name + "/seq_ratio_volume.dat", "r")
    
    line = seq_file.readline()
    line = seq_file.readline()
    
    while line:
        var_seq_change = 0
        cate = 0
        big_seq = float(line.split()[1])
        mid_seq = float(line.split()[2])
        small_seq = float(line.split()[3])

        if abs(big_seq - mid_seq) > CHANGE_RATIO or \
        abs(mid_seq - small_seq) > CHANGE_RATIO or \
        abs(big_seq - small_seq) > CHANGE_RATIO:
            var_seq_change = 1
        vars_seq_big.append(big_seq)
        vars_seq_mid.append(mid_seq)
        vars_seq_small.append(small_seq)
        vars_seq_change.append(var_seq_change)

        if big_seq > 75:
            cate = 1
        else:
            cate = 0
        vars_seq_cate.append(cate)
        line = seq_file.readline()

def GetStrideRatio(app_name):
    stride_file = open(app_name + "/stride_ratio_volume.dat", "r")
    line = stride_file.readline()
    line = stride_file.readline()

    while line:
        var_stride_change = 0
        cate = 0
        big_stride = float(line.split()[1])
        mid_stride = float(line.split()[2])
        small_stride = float(line.split()[3])
        if abs(big_stride - mid_stride) > CHANGE_RATIO or \
        abs(mid_stride - small_stride) > CHANGE_RATIO or \
        abs(big_stride - small_stride) > CHANGE_RATIO:
            var_stride_change = 1
        vars_stride_big.append(big_stride)
        vars_stride_mid.append(mid_stride)
        vars_stride_small.append(small_stride)
        vars_stride_change.append(var_stride_change)
        #print line
        if big_stride > 50:
            cate = 2
        else:
            cate = 0
        vars_stride_cate.append(cate)
        line = stride_file.readline()
        


app_file = open("scientific_apps", "r")
app_names = []
app_count = []

line = app_file.readline()
while line:
    line = line.strip("\n")
    GetCate(line)
    GetSize(line)
    GetReadRatio(line)
    GetSeqRatio(line)
    GetStrideRatio(line)
    GetDensityRatio(line)
    GetTmpPageRatio(line)
    GetTmpCacheRatio(line)
    GetSpaCacheRatio(line)
    GetSpaPageRatio(line)
    
    #print line, len(cate_array), len(vars_read_change) 
    app_names.append(line)
    app_count.append(len(cate_array))
    #print line

    line = app_file.readline()

#summary the results
missing_count = 0
cate_scaling = [0] * 10
cate_count = [0] * 10
cate_fixed = [0] * 10
cate_scaling = [0] * 10
cate_irrgular = [0] * 10
#cate_missing = [0] * 10
cate_read_change = [0] * 10
cate_read_big = [0.0] * 10
cate_read_mid = [0.0] * 10
cate_read_small = [0.0] * 10
cate_seq_change = [0] * 10
cate_seq_big = [0.0] * 10
cate_seq_mid = [0.0] * 10
cate_seq_small = [0.0] * 10
cate_stride_change = [0] * 10
cate_stride_big = [0.0] * 10
cate_stride_mid = [0.0] * 10
cate_stride_small = [0.0] * 10
cate_density_scaling = [0.0] * 10
cate_density_fixed = [0.0] * 10
cate_density_irrgular = [0.0] * 10
cate_tmp_page_change = [0.0] * 10
cate_tmp_cache_change = [0.0] * 10
cate_spa_cache_change = [0.0] * 10
cate_spa_page_change = [0.0] * 10
cate_density_scaling = [0.0] * 10
cate_density_scaling = [0.0] * 10
total_reads = []
total_tmp_locs = []
total_spa_locs = []

j = 0
fixed = 0
change = 0
irrgular = 0
scaling = 0
cate_densitys = []
cate_reads = []
cate_seqs = []
cate_tmp_locs = []
cate_spa_locs = []

for i in range(0, 10):
    cate_seqs.append([])
    cate_reads.append([])
    cate_tmp_locs.append([])
    cate_spa_locs.append([])
density_app = []
#print len(cate_array)
for i in range(0, len(cate_array)+1):
    last_i = 0
    if i == app_count[j]:
        #print app_count[j] - app_count[j-1]
        last_i = i
        #print "here"
        #print j
        #print app_names[j], density_app.count(1), density_app.count(2), density_app.count(3), density_app.count(4), density_app.count(5)
        fixed = 0
        change = 0
        scaling = 0
        irrgular = 0
        density_app = []
        print app_names[j]
        j += 1

    if i == len(cate_array):
        break
    print len(cate_array)
    if vars_missing[i] == 1:
        missing_count += 1
        print "missing", i
        continue
    if vars_large[i] == 0:
        continue
        
    density_app.append(vars_density_cate[i])
    
    cate_id = cate_array[i]
    cate_count[cate_id] += 1
    #cate_densitys[cate_id].append(vars_density_cate[i])
    if vars_density_big[i] == 0:
        vars_read_cate[i] = 3
        vars_tmp_loc[i] = 0
        vars_spa_loc[i] = 0
    
    total_reads.append(vars_read_cate[i])
    total_tmp_locs.append(vars_tmp_loc[i])
    total_spa_locs.append(vars_spa_loc[i])
    if vars_seq_big[i] + vars_stride_big[i] < 25:
        vars_seq_cate[i] = 4
        #print "Random!", i
    #print i
    cate_seqs[cate_id].append(vars_seq_cate[i] + vars_stride_cate[i])
    
    
    #cate_spa_locs[cate_id].append(vars_spa_loc[i])
    #print vars_tmp_loc[i]
    #print vars_seq_cate[i], vars_stride_cate[i]
    #vars_density_big.append(big_density)
    #print "here"
    #if cate_id == 2 and vars_scaling[i] == 1:
    #    print "S vector!!!", i
    #if vars_read_change[i] == 1:
    #    change += 1
    #else:
    #    fixed += 1

    #vars_density_cate
    if vars_spa_cache_change[i] == 1:
        change += 1
    #elif vars_stride_change[i] == 1:
    #    change += 1
    elif vars_spa_cache_change[i] == 0:
        fixed += 1

    #print vars_stride_change[i]
    #if vars_density_fixed[i] == 1:
    #    fixed += 1
    #print app_count, i, j
    #print i, app_count[0]
   
    cate_fixed[cate_id] += vars_fixed[i]
    cate_scaling[cate_id] += vars_scaling[i]
    cate_irrgular[cate_id] += vars_irrgular[i]

    cate_read_change[cate_id] += vars_read_change[i]
    cate_read_big[cate_id] += vars_read_big[i]
    cate_read_mid[cate_id] += vars_read_mid[i]
    cate_read_small[cate_id] += vars_read_small[i]

    cate_seq_change[cate_id] += vars_seq_change[i]
    cate_seq_big[cate_id] += vars_seq_big[i]
    cate_seq_mid[cate_id] += vars_seq_mid[i]
    cate_seq_small[cate_id] += vars_seq_small[i]

    cate_stride_change[cate_id] += vars_stride_change[i]
    cate_stride_big[cate_id] += vars_stride_big[i]
    cate_stride_mid[cate_id] += vars_stride_mid[i]
    cate_stride_small[cate_id] += vars_stride_small[i]

    cate_density_fixed[cate_id] += vars_density_fixed[i]
    cate_density_irrgular[cate_id] += vars_density_irrgular[i]
    cate_density_scaling[cate_id] += vars_density_scaling[i]
    cate_tmp_page_change[cate_id] += vars_tmp_page_change[i]
    cate_tmp_cache_change[cate_id] += vars_tmp_cache_change[i]
    cate_spa_cache_change[cate_id] += vars_spa_page_change[i]
    cate_spa_page_change[cate_id] += vars_spa_cache_change[i]
    cate_density_scaling[cate_id] += vars_density_scaling[i]


f = open("density_res", "w")

def output(list):
    #print len(list)
    print list.count(0), list.count(1), list.count(2), list.count(3) ,list.count(4) #, list.count(5)
    #for i in range(0, len(list)):
    #    f.write(str(list[i]) + "\t")
    #f.write("\n")


#output(cate_seqs[0])
#output(cate_seqs[1])
#output(cate_seqs[2])
#output(cate_seqs[3])
#output(cate_seqs[4])
#output(cate_seqs[5])
#output(cate_seqs[6])
#output(cate_seqs[7])
#output(cate_seqs[8])

#output(total_reads)
output(total_spa_locs)

'''
cate_avg_read_big = [0.0] * 10
cate_avg_read_mid = [0.0] * 10
cate_avg_read_small = [0.0] * 10

cate_avg_seq_big = [0.0] * 10
cate_avg_seq_mid = [0.0] * 10
cate_avg_seq_small = [0.0] * 10

cate_avg_stride_big = [0.0] * 10
cate_avg_stride_mid = [0.0] * 10
cate_avg_stride_small = [0.0] * 10

for i in range(0, 10):
    if cate_count[i] != 0:
        cate_avg_read_big[i] = cate_read_big[i] / cate_count[i]
        cate_avg_read_mid[i] = cate_read_mid[i] / cate_count[i]
        cate_avg_read_small[i] = cate_read_small[i] / cate_count[i]
    
        cate_avg_seq_big[i] = cate_seq_big[i] / cate_count[i]
        cate_avg_seq_mid[i] = cate_seq_mid[i] / cate_count[i]
        cate_avg_seq_small[i] = cate_seq_small[i] / cate_count[i]
        
        cate_avg_stride_big[i] = cate_stride_big[i] / cate_count[i]
        cate_avg_stride_mid[i] = cate_stride_mid[i] / cate_count[i]
        cate_avg_stride_small[i] = cate_stride_small[i] / cate_count[i]

'''        
'''
print 'stride_total_big', cate_stride_big[0], cate_stride_big[1], cate_stride_big[2], \
    cate_stride_big[3], cate_stride_big[4], cate_stride_big[5], \
    cate_stride_big[6], cate_stride_big[7], cate_stride_big[8]

print 'stride_big', cate_avg_stride_big[0], cate_avg_stride_big[1], cate_avg_stride_big[2], \
    cate_avg_stride_big[3], cate_avg_stride_big[4], cate_avg_stride_big[5], \
    cate_avg_stride_big[6], cate_avg_stride_big[7], cate_avg_stride_big[8]


print "missing", missing_count
print 'count', cate_count[0], cate_count[1], cate_count[2], \
    cate_count[3], cate_count[4], cate_count[5], \
    cate_count[6], cate_count[7], cate_count[8]

print 'fixed', cate_fixed[0], cate_fixed[1], cate_fixed[2], \
    cate_fixed[3], cate_fixed[4], cate_fixed[5], \
    cate_fixed[6], cate_fixed[7], cate_fixed[8]

print 'scaling', cate_scaling[0], cate_scaling[1], cate_scaling[2], \
    cate_scaling[3], cate_scaling[4], cate_scaling[5], \
    cate_scaling[6], cate_scaling[7], cate_scaling[8]

print 'irrgular', cate_irrgular[0], cate_irrgular[1], cate_irrgular[2], \
    cate_irrgular[3], cate_irrgular[4], cate_irrgular[5], \
    cate_irrgular[6], cate_irrgular[7], cate_irrgular[8]

print 'read_change', cate_read_change[0], cate_read_change[1], cate_read_change[2], \
    cate_read_change[3], cate_read_change[4], cate_read_change[5], \
    cate_read_change[6], cate_read_change[7], cate_read_change[8]

print 'density_fixed', cate_density_fixed[0], cate_density_fixed[1], cate_density_fixed[2], \
    cate_density_fixed[3], cate_density_fixed[4], cate_density_fixed[5], \
    cate_density_fixed[6], cate_density_fixed[7], cate_density_fixed[8]

print 'denisty_scaling', cate_denisty_scaling[0], cate_denisty_scaling[1], cate_denisty_scaling[2], \
    cate_denisty_scaling[3], cate_denisty_scaling[4], cate_denisty_scaling[5], \
    cate_denisty_scaling[6], cate_denisty_scaling[7], cate_denisty_scaling[8]

print 'density_irrgular', cate_density_irrgular[0], cate_density_irrgular[1], cate_density_irrgular[2], \
    cate_density_irrgular[3], cate_density_irrgular[4], cate_density_irrgular[5], \
    cate_density_irrgular[6], cate_density_irrgular[7], cate_density_irrgular[8]

print 'tmp_cache', cate_tmp_cache[0], cate_tmp_cache[1], cate_tmp_cache[2], \
    cate_tmp_cache[3], cate_tmp_cache[4], cate_tmp_cache[5], \
    cate_tmp_cache[6], cate_tmp_cache[7], cate_tmp_cache[8]

print 'seq_change', cate_seq_change[0], cate_seq_change[1], cate_seq_change[2], \
    cate_seq_change[3], cate_seq_change[4], cate_seq_change[5], \
    cate_seq_change[6], cate_seq_change[7], cate_seq_change[8]

print 'seq_change', cate_seq_change[0], cate_seq_change[1], cate_seq_change[2], \
    cate_seq_change[3], cate_seq_change[4], cate_seq_change[5], \
    cate_seq_change[6], cate_seq_change[7], cate_seq_change[8]

print 'seq_change', cate_seq_change[0], cate_seq_change[1], cate_seq_change[2], \
    cate_seq_change[3], cate_seq_change[4], cate_seq_change[5], \
    cate_seq_change[6], cate_seq_change[7], cate_seq_change[8]

'''

'''
print 'read_big', cate_avg_read_big[0], cate_avg_read_big[1], cate_avg_read_big[2], \
    cate_avg_read_big[3], cate_avg_read_big[4], cate_avg_read_big[5], \
    cate_avg_read_big[6], cate_avg_read_big[7], cate_avg_read_big[8]

print 'read_mid', cate_avg_read_mid[0], cate_avg_read_mid[1], cate_avg_read_mid[2], \
    cate_avg_read_mid[3], cate_avg_read_mid[4], cate_avg_read_mid[5], \
    cate_avg_read_mid[6], cate_avg_read_mid[7], cate_avg_read_mid[8]

print 'read_small', cate_avg_read_small[0], cate_avg_read_small[1], cate_avg_read_small[2], \
    cate_avg_read_small[3], cate_avg_read_small[4], cate_avg_read_small[5], \
    cate_avg_read_small[6], cate_avg_read_small[7], cate_avg_read_small[8]

print 'seq_change', cate_seq_change[0], cate_seq_change[1], cate_seq_change[2], \
    cate_seq_change[3], cate_seq_change[4], cate_seq_change[5], \
    cate_seq_change[6], cate_seq_change[7], cate_seq_change[8]

print 'seq_big', cate_avg_seq_big[0], cate_avg_seq_big[1], cate_avg_seq_big[2], \
    cate_avg_seq_big[3], cate_avg_seq_big[4], cate_avg_seq_big[5], \
    cate_avg_seq_big[6], cate_avg_seq_big[7], cate_avg_seq_big[8]

print 'seq_mid', cate_avg_seq_mid[0], cate_avg_seq_mid[1], cate_avg_seq_mid[2], \
    cate_avg_seq_mid[3], cate_avg_seq_mid[4], cate_avg_seq_mid[5], \
    cate_avg_seq_mid[6], cate_avg_seq_mid[7], cate_avg_seq_mid[8]

print 'seq_small', cate_avg_seq_small[0], cate_avg_seq_small[1], cate_avg_seq_small[2], \
    cate_avg_seq_small[3], cate_avg_seq_small[4], cate_avg_seq_small[5], \
    cate_avg_seq_small[6], cate_avg_seq_small[7], cate_avg_seq_small[8]

print 'stride_change', cate_stride_change[0], cate_stride_change[1], cate_stride_change[2], \
    cate_stride_change[3], cate_stride_change[4], cate_stride_change[5], \
    cate_stride_change[6], cate_stride_change[7], cate_stride_change[8]

print 'stride_big', cate_avg_stride_big[0], cate_avg_stride_big[1], cate_avg_stride_big[2], \
    cate_avg_stride_big[3], cate_avg_stride_big[4], cate_avg_stride_big[5], \
    cate_avg_stride_big[6], cate_avg_stride_big[7], cate_avg_stride_big[8]

print 'stride_mid', cate_avg_stride_mid[0], cate_avg_stride_mid[1], cate_avg_stride_mid[2], \
    cate_avg_stride_mid[3], cate_avg_stride_mid[4], cate_avg_stride_mid[5], \
    cate_avg_stride_mid[6], cate_avg_stride_mid[7], cate_avg_stride_mid[8]

print 'stride_small', cate_avg_stride_small[0], cate_avg_stride_small[1], cate_avg_stride_small[2], \
    cate_avg_stride_small[3], cate_avg_stride_small[4], cate_avg_stride_small[5], \
    cate_avg_stride_small[6], cate_avg_stride_small[7], cate_avg_stride_small[8]

'''
