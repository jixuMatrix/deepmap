set term eps 
set output outputfile
set style data histogram
set style histogram clustered gap 1
set style fill solid 1 border 0
 
#set style fill pattern 0  border 1
set xlabel "Variable ID_Object ID" font ",24"
set ylabel "Spatial locality in cache" font ",21"
set key font ",19"
#set title "Varible size of bzip2"
#set logscale y
#set format y "%.0e"
set key top right
#set style histogram rowstacked
set boxwidth 0.8 relative
set xtics rotate by -45
set yrange [0:1.2]

plot inputfile using ($4/100):xticlabels(1) title columnheader(2) linecolor rgb "#000000", \
'' using ($3/100):xticlabels(1) title columnheader(3) linecolor rgb "#FFFFFF", \
'' using ($2/100):xticlabels(1) title columnheader(4) linecolor rgb "#BEBEBE"
#'' using 5:xticlabels(1) title columnheader(5) linecolor rgb "#90EE90", \
#'' using 6:xticlabels(1) title columnheader(6) linecolor rgb "#FF0000"
