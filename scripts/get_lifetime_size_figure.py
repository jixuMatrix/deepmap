# This script is for getting the original lifetime figure(not used) or the corelation between sizes and lifetimes

import sys
import math
import statsmodels.api as sm
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib
from optparse import OptionParser
import os

color = []
id_points = []
begin_time_points = []
end_time_points = []
total_obj = 0
var_ids = []
var_sizes = []

color_count = 0
spec_input = ["ref", "train", "test"]
parsec_input = ["native", "simlarge", "test"]
obj_address = {}

start_time = 0.0
end_time = 0.0
trace_size = 0

#CHANGEME
#For getting the coresspoding density and the lifetime
'''
var_sizes = [0] * 20
begin_time_points = [0.0] * 20
end_time_points = [0.0] * 20
'''

spec_name = ["400.perlbench",
"401.bzip2",
"403.gcc",
"410.bwaves",
"416.gamess",
"429.mcf",
"433.milc",
"434.zeusmp",
"435.gromacs",
"436.cactusAD",
"437.leslie3d",
"444.namd",
"445.gobmk",
"447.dealII",
"450.soplex",
"453.povray",
"454.calculix",
"456.hmmer",
"458.sjeng",
"459.GemsFDTD",
"462.libquantum",
"464.h264ref",
"465.tonto",
"470.lbm",
"471.omnetpp",
"473.astar",
"481.wrf",
"482.sphinx3",
"483.xalancbmk"
]

parser = OptionParser()

parser.add_option("-i", "--input", dest="inputdir",
                  help = "input directory")
parser.add_option("-o", "--output", dest="outputdir",
                  help = "output directory")
parser.add_option("-m", "--model", dest="model",
                  help = "model: SPEC, parsec", default = "SPEC")
parser.add_option("-n", "--name", dest="task_name",
                  help = "the task name")


(options, args) = parser.parse_args()

def init_color ():
    color.append("#0000FF")
    color.append("#FF0000")
    color.append("#000000")
    color.append("#90EE90")
    color.append("#90EE90")
    color.append("#000000")

init_color()

def find_name(task_name):
    task_name = str(task_name)
    task_name = task_name[:task_name.find("-")]
    for i in range(0, len(spec_name)):
        if task_name in spec_name[i]:
            task_name = spec_name[i][spec_name[i].find(".")+1:]
            break
    return task_name

def HandleAlloc(obj_id, var_id, ptr, size, time, var_ids):
    global id_points
    global time_points
    global total_obj

    for i in range(0, len(var_ids)):
        #CHANGME 
        # modify for temperorily getting sorted varID variables' information
        #print var_ids[i]
        #print trace_size
        #print size, trace_size
        if var_id == var_ids[i] and size > var_sizes[i] and size >= trace_size:
            begin_time_points[i] = time
            end_time_points[i] = time
            var_sizes[i] = size
            #id_points.append(i+1)
            obj_address[ptr] = i
            #total_obj = total_obj + 1


    '''    
    for i in range(0, len(var_ids)):
        #CHANGME
        # modify for temperorily getting all the variables' information
        if var_id == var_ids[i]: 
            begin_time_points.append(time)
            end_time_points.append(time)
            var_sizes.append(size)
            id_points.append(i+1)
            obj_address[ptr] = total_obj
            total_obj = total_obj + 1
    '''
    '''
    #CHANGME
    # modify for temperorily getting all the variables' information
    begin_time_points.append(time)
    end_time_points.append(time)
    var_sizes.append(size)
    id_points.append(var_id)
    obj_address[ptr] = total_obj
    total_obj = total_obj + 1
    #print var_id
    '''

def HandleFree(ptr, time):
    global end_time_points
    try:
        end_time_points[obj_address[ptr]] = time
        obj_address[ptr] = -1
    except:
        a = 1
        #print "What?!"

def work(dirname, input_name):
    global start_time
    global end_time
    global footprint_points
    global time_points
    global footprint
    global var_ids
    global trace_size
    var_ids = []
    
    #for getting the lifetime information according to the variIDs
    
    id_file = open(options.inputdir + "/" + dirname + "/sorted_VarIDs", "r")

    line = id_file.readline()
    while line:
        var_ids.append(int(line))
        line = id_file.readline()
    '''
    try:
        trace_size_file = open(options.inputdir + "/" + dirname + "/TraceSize", "r")
        line = trace_size_file.readline()
    
        trace_size = int(line)
    except:
        trace_size = 0
    '''
    #print trace_size
    
    
    raw_file = open(options.inputdir + "/" + dirname + "/var_size_file", "r")
    time_points = []
    footprint_points = []
    footprint = 0

    line = raw_file.readline()

    while line:
        #print line
        if "malloc" in line:
            obj_id = int(line[line.find("malloc:") + 7:line.find("var_id")])
            var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
            ptr = line[line.find("ptr:") + 4:line.find("size")]
            size = int(line[line.find("size:") + 5:line.find("times")])
            time = float(line[line.find("times:") + 6:].replace('\n', ''))
        
            HandleAlloc(obj_id, var_id, ptr, size, time, var_ids)

        if "calloc" in line:
            obj_id = int(line[line.find("calloc:") + 7:line.find("var_id")])
            var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
            ptr = line[line.find("ptr:") + 4:line.find("size")]
            size = int(line[line.find("size:") + 5:line.find("times")])
            time = float(line[line.find("times:") + 6:].replace('\n', ''))
            #print "here"
            HandleAlloc(obj_id, var_id, ptr, size, time, var_ids)

        if "realloc" in line:
            obj_id = int(line[line.find("realloc:") + 8:line.find("var_id")])
            var_id = int(line[line.find("var_id:") + 7:line.find("ptr:")])
            ptr = line[line.find("ptr:") + 4:line.find("before")]
            size = int(line[line.find("size:") + 5:line.find("times")])
            pre_ptr = line[line.find("before:") + 7:line.find("size")]
            time = float(line[line.find("times:") + 6:].replace('\n', ''))
            
            HandleFree(pre_ptr, time)
            HandleAlloc(obj_id, var_id, ptr, size, time, var_ids)

        if "free" in line:
            ptr = line[line.find("ptr:") + 4:line.find("times")]
            time = float(line[line.find("times:") + 6:].replace('\n', ''))

            HandleFree(ptr, time)

        if "times:" in line:
            last_time = float(line[line.find("times:") + 6:].replace('\n', ''))

        if "Start" in line:
            start_time = float(line[line.find("time:") + 5:].replace('\n', ''))

        if "End" in line:
            end_time = float(line[line.find("time:") + 5:].replace('\n', ''))
            
        line = raw_file.readline()
    
    for i in range(0, len(time_points)):
        time_points[i] = (time_points[i] - start_time) / (end_time - start_time)
    #size_plot(time_points, footprint_points, input_name)


#for getting the lifetime information according to the variIDs
'''
work(".", "ref")
task_name = find_name(options.task_name)
outfile = open(options.outputdir + "/" + task_name + "_single_lifetime", "w")
outfile.write("lifetimes\n")
for i in range(0, 10):
    if var_sizes[i] > 0:
        if end_time_points[i] == begin_time_points[i]:
            end_time_points[i] = end_time
        outfile.write(str((end_time_points[i] - begin_time_points[i]) / (end_time - start_time)) + "\n")

#for getting all sizes and lifetime information
'''

work(".", "ref")
  
task_name = find_name(options.task_name)

outfile = open(options.outputdir + "/" + task_name + "_lifetime", "w")
avg_lifetime = [0.0] * 20000
var_counts = [0] * 20000
total_size = [0] * 20000
max_var_id = 0

for i in range(0, len(id_points)):
    if end_time_points[i] == begin_time_points[i]:
        end_time_points[i] = end_time
    #if id_points[i] < 0 or id_points[i] >= 20000:
    #    continue
    #print id_points[i], var_sizes[i], begin_time_points[i], end_time_points[i]
    avg_lifetime[id_points[i]] += ((end_time_points[i] - begin_time_points[i]) / (end_time - start_time))
    var_counts[id_points[i]] += 1
    total_size[id_points[i]] += var_sizes[i]
    if id_points[i] > max_var_id:
        max_var_id = id_points[i]
    #print id_points[i]
#for i in range(1, max_var_id + 1):
#    if var_counts[i] == 0:
#        var_counts[i] = 1
#outfile.write("Here\n");

for i in range(1, max_var_id + 1):
    if var_counts[i] != 0:
        outfile.write(str(float(total_size[i]) / var_counts[i]) + "\t" + str(avg_lifetime[i] / var_counts[i]) + "\n")


# for drawing the lifetime figures

'''
bar_counts = [0] * 20;
print len(id_points)
for i in range(0, len(id_points)):
    bar_counts[id_points[i]] += 1
    if bar_counts[id_points[i]] < 10 or bar_counts[id_points[i]] % 100 == 0:
        plt.barh(id_points[i], ((end_time_points[i] - begin_time_points[i]) / (end_time - start_time)), color = 'white', left = ((begin_time_points[i] - start_time) / (end_time - start_time)), height = 0.3)
'''
 
'''
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 18}

matplotlib.rc('font', **font)

plt.ylabel("Variable ID", fontsize = 24)
#plt.ylim([1, len(var_ids) + 1])
ax = plt.gca()
ax.set_yticks(range(1, len(var_ids) + 1))

white_patch = mpatches.Patch(facecolor = 'white', label='Object')
plt.legend(handles=[white_patch], fontsize=18, frameon=False)

plt.xlabel("Normalized execution time", fontsize = 24)
#plt.legend(bbox_to_anchor=(0.6, 0.4), loc=2, borderaxespad=0.)
plt.savefig(options.outputdir + "/" + task_name + "_lifetime.eps", format="eps", dpi=300)
plt.clf()
'''
