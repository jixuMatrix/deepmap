set term eps 
set output outputfile
set style data histogram
set style fill solid 1 border 0
#set style histogram clustered gap 0.5
set style histogram rowstacked

#set style fill pattern 0  border 1
#set title "Varible size of bzip2"
set format y "%g%%"
set key font ",21" width 4.5 #height 1
#set key font ",20" width 2.0 #spacing 2.0
#set yrange [0:115]
set yrange [0:115]
set key right top 
#set key at 10, 10
set key horizontal #samplen 1

#set style histogram rowstacked
set boxwidth 0.72 relative
set xtics font ",24"   
set ytics font ",24"
set bmargin at screen 0.28 
set lmargin at screen 0.18
set xtics rotate by -45 
#filename = "seq_ratio.dat"
plot inputfile using 2:xticlabels(1) title columnheader(2) linecolor rgb "#000000", \
'' using 3:xticlabels(1) title columnheader(3) linecolor rgb "#FFFFFF", \
'' using 4:xticlabels(1) title columnheader(4) linecolor rgb "#BEBEBE"
#'' using 5:xticlabels(1) title columnheader(5) linecolor rgb "#90EE90", \
#'' using 6:xticlabels(1) title columnheader(6) linecolor rgb "#000000" fillstyle pattern 5
##"000000" fillstyle pattern 1, \

