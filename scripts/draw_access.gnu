set terminal postscript solid linewidth 2 font "Helvetica, 16" 
set term eps 
set output "access.eps"
set style data histogram
set style histogram clustered gap 1
#set style fill solid 0.7 border
#set style fill pattern 1 border lt -1
set style fill solid 1  border 0
#set style fill transparent solid 0.4

set xlabel "Variable ID_Object ID"
#set ylabel "size in MB"
#set logscale y
#set format y "%.0e"
#set title "Normalized total access volume of bzip2"

set key 
set bar fullwidth
set style histogram rowstacked
set boxwidth 0.5
#set xtics rotate by -45
unset xtics
#unset key
#set key maxrows 2 
#set key samplen 2
#set key opaque
#set x2tics rotate by -45

#plot "access.dat" \
#using ($2+$3):xticlabels(1) title columnheader(2) with boxes,'' using 3:xticlabels(1) title columnheader(3) with boxes,\
# '' using ($4+$5):xticlabels(1) title columnheader(4) with boxes, '' using 5::xticlabels(1) title columnheader(5) with boxes, \
# '' using ($6+$7):xticlabels(1) title columnheader(6) with boxes, '' using 7:xticlabels(1) title columnheader(7) with boxes, \
# '' using ($8+$9):xticlabels(1) title columnheader(8) with boxes, '' using 9:xticlabels(1) title columnheader(9) with boxes, \
# '' using ($10+$11):xticlabels(1) title columnheader(10) with boxes, '' using 11:xticlabels(1) title columnheader(11) with boxes

#plot "access.dat.bak" \
#using ($2+$3):xticlabels(1) title columnheader(2), '' using 3:xticlabels(1) title columnheader(3),\
# '' using ($4+$5):xticlabels(1) title columnheader(4), '' using 5:xticlabels(1) title columnheader(5), \
# '' using ($6+$7):xticlabels(1) title columnheader(6), '' using 7:xticlabels(1) title columnheader(7), \
# '' using ($8+$9):xticlabels(1) title columnheader(8), '' using 9:xticlabels(1) title columnheader(9), \
# '' using ($10+$11):xticlabels(1) title columnheader(10), '' using 11:xticlabels(1) title columnheader(11)

#plot "access.dat" using 2:xticlabels(1) title columnheader(2), '' using 3:xticlabels(1) title columnheader(3), '' using 4:xticlabels(1) title columnheader(4), '' using 5:xticlabels(1) title columnheader(5), '' using 6:xticlabels(1) title columnheader(6), '' using 7:xticlabels(1) title columnheader(7), '' using 8:xticlabels(1) title columnheader(8), '' using 9:xticlabels(1) title columnheader(9), '' using 10:xticlabels(1) title columnheader(10), '' using 11:xticlabels(1) title columnheader(11)
#plot newhistogram "combined-5MB", "access.dat" using 2:xticlabels(1) title columnheader(2), '' using 3:xticlabels(1) title columnheader(3), newhistogram "source-5MB", '' using 4:xticlabels(1) title columnheader(4), '' using 5:xticlabels(1) title columnheader(5), newhistogram "program-5MB", '' using 6:xticlabels(1) title columnheader(6), '' using 7:xticlabels(1) title columnheader(7), newhistogram "program-10MB", '' using 8:xticlabels(1) title columnheader(8), '' using 9:xticlabels(1) title columnheader(9), newhistogram "program-10MB", '' using 10:xticlabels(1) title columnheader(10), '' using 11:xticlabels(1) title columnheader(11)

plot newhistogram "1_1", "access_final_data" using 2 title "combined-5MB" linecolor rgb "#000000", \
'' using 3 title "source-5MB" linecolor rgb "#FFFFFF", \
'' using 4 title "program-5MB" linecolor rgb "#BEBEBE", \
'' using 5 title "program-10MB" linecolor rgb "#90EE90", \
'' using 6 title "program-30MB" linecolor rgb "#FF0000", \
'' using 7 title "Write" linecolor rgb "#000000" fill pattern 0, \
newhistogram "2_1", '' using 8 title "" linecolor rgb "#000000", \
'' using 9 title "" linecolor rgb "#FFFFFF", \
'' using 10 title "" linecolor rgb "#BEBEBE", \
'' using 11 title "" linecolor rgb "#90EE90", \
'' using 12 title "" linecolor rgb "#FF0000", \
'' using 13 title "" linecolor rgb "#000000" fill pattern 0, \
newhistogram "3_1", '' using 14 title "" linecolor rgb "#000000", \
'' using 15 title "" linecolor rgb "#FFFFFF", \
'' using 16 title "" linecolor rgb "#BEBEBE", \
'' using 17 title "" linecolor rgb "#90EE90", \
'' using 18 title "" linecolor rgb "#FF0000", \
'' using 19 title "" linecolor rgb "#000000" fill pattern 0, \
newhistogram "4_1", '' using 38 title "" linecolor rgb "#000000", \
'' using 39 title "" linecolor rgb "#FFFFFF", \
'' using 40 title "" linecolor rgb "#BEBEBE", \
'' using 41 title "" linecolor rgb "#90EE90", \
'' using 42 title "" linecolor rgb "#FF0000", \
'' using 43 title "" linecolor rgb "#000000" fill pattern 0, \
newhistogram "5_1", '' using 56 title "" linecolor rgb "#000000", \
'' using 57 title "" linecolor rgb "#FFFFFF", \
'' using 58 title "" linecolor rgb "#BEBEBE", \
'' using 59 title "" linecolor rgb "#90EE90", \
'' using 60 title "" linecolor rgb "#FF0000", \
'' using 61 title "" linecolor rgb "#000000" fill pattern 0, \
newhistogram "5_2", '' using 62 title "" linecolor rgb "#000000", \
'' using 63 title "" linecolor rgb "#FFFFFF", \
'' using 64 title "" linecolor rgb "#BEBEBE", \
'' using 65 title "" linecolor rgb "#90EE90", \
'' using 66 title "" linecolor rgb "#FF0000", \
'' using 67 title "" linecolor rgb "#000000" fill pattern 0, \
newhistogram "5_3", '' using 68 title "" linecolor rgb "#000000", \
'' using 69 title "" linecolor rgb "#FFFFFF", \
'' using 70 title "" linecolor rgb "#BEBEBE", \
'' using 71 title "" linecolor rgb "#90EE90", \
'' using 72 title "" linecolor rgb "#FF0000", \
'' using 73 title "" linecolor rgb "#000000" fill pattern 0, \
newhistogram "6_1", '' using 110 title "" linecolor rgb "#000000", \
'' using 111 title "" linecolor rgb "#FFFFFF", \
'' using 112 title "" linecolor rgb "#BEBEBE", \
'' using 113 title "" linecolor rgb "#90EE90", \
'' using 114 title "" linecolor rgb "#FF0000", \
'' using 115 title "" linecolor rgb "#000000" fill pattern 0, \

#plot newhistogram "1_1", "access_final_data" using 2 title "combined-5MB" linecolor rgb "#000000", \
#'' using 3 title "source-5MB" linecolor rgb "#FFFFFF", \
#'' using 4 title "program-5MB" linecolor rgb "#BEBEBE", \
#'' using 5 title "program-10MB" linecolor rgb "#90EE90", \
#'' using 6 title "program-30MB" linecolor rgb "#FF00000", \
#'' using 7 title "Write" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "2_1", '' using 8 title "" linecolor rgb "#000000", \
#'' using 9 title "" linecolor rgb "#FFFFFF", \
#'' using 10 title "" linecolor rgb "#BEBEBE", \
#'' using 11 title "" linecolor rgb "#90EE90", \
#'' using 12 title "" linecolor rgb "#FF0000", \
#'' using 13 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "3_1", '' using 14 title "" linecolor rgb "#000000", \
#'' using 15 title "" linecolor rgb "#FFFFFF", \
#'' using 16 title "" linecolor rgb "#BEBEBE", \
#'' using 17 title "" linecolor rgb "#90EE90", \
#'' using 18 title "" linecolor rgb "#FF0000", \
#'' using 19 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "4_6", '' using 20 title "" linecolor rgb "#000000", \
#'' using 21 title "" linecolor rgb "#FFFFFF", \
#'' using 22 title "" linecolor rgb "#BEBEBE", \
#'' using 23 title "" linecolor rgb "#90EE90", \
#'' using 24 title "" linecolor rgb "#FF0000", \
#'' using 25 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "4_14", '' using 26 title "" linecolor rgb "#000000", \
#'' using 27 title "" linecolor rgb "#FFFFFF", \
#'' using 28 title "" linecolor rgb "#BEBEBE", \
#'' using 29 title "" linecolor rgb "#90EE90", \
#'' using 30 title "" linecolor rgb "#FF0000", \
#'' using 31 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "4_22", '' using 32 title "" linecolor rgb "#000000", \
#'' using 33 title "" linecolor rgb "#FFFFFF", \
#'' using 34 title "" linecolor rgb "#BEBEBE", \
#'' using 35 title "" linecolor rgb "#90EE90", \
#'' using 36 title "" linecolor rgb "#FF0000", \
#'' using 37 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "5_7", '' using 38 title "" linecolor rgb "#000000", \
#'' using 39 title "" linecolor rgb "#FFFFFF", \
#'' using 40 title "" linecolor rgb "#BEBEBE", \
#'' using 41 title "" linecolor rgb "#90EE90", \
#'' using 42 title "" linecolor rgb "#FF0000", \
#'' using 43 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "5_15", '' using 44 title "" linecolor rgb "#000000", \
#'' using 45 title "" linecolor rgb "#FFFFFF", \
#'' using 46 title "" linecolor rgb "#BEBEBE", \
#'' using 47 title "" linecolor rgb "#90EE90", \
#'' using 48 title "" linecolor rgb "#FF0000", \
#'' using 49 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "5_22", '' using 50 title "" linecolor rgb "#000000", \
#'' using 51 title "" linecolor rgb "#FFFFFF", \
#'' using 52 title "" linecolor rgb "#BEBEBE", \
#'' using 53 title "" linecolor rgb "#90EE90", \
#'' using 54 title "" linecolor rgb "#FF0000", \
#'' using 55 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "6_8", '' using 56 title "" linecolor rgb "#000000", \
#'' using 57 title "" linecolor rgb "#FFFFFF", \
#'' using 58 title "" linecolor rgb "#BEBEBE", \
#'' using 59 title "" linecolor rgb "#90EE90", \
#'' using 60 title "" linecolor rgb "#FF0000", \
#'' using 61 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "6_16", '' using 62 title "" linecolor rgb "#000000", \
#'' using 63 title "" linecolor rgb "#FFFFFF", \
#'' using 64 title "" linecolor rgb "#BEBEBE", \
#'' using 65 title "" linecolor rgb "#90EE90", \
#'' using 66 title "" linecolor rgb "#FF0000", \
#'' using 67 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "6_24", '' using 68 title "" linecolor rgb "#000000", \
#'' using 69 title "" linecolor rgb "#FFFFFF", \
#'' using 70 title "" linecolor rgb "#BEBEBE", \
#'' using 71 title "" linecolor rgb "#90EE90", \
#'' using 72 title "" linecolor rgb "#FF0000", \
#'' using 73 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "7_9", '' using 74 title "" linecolor rgb "#000000", \
#'' using 75 title "" linecolor rgb "#FFFFFF", \
#'' using 76 title "" linecolor rgb "#BEBEBE", \
#'' using 77 title "" linecolor rgb "#90EE90", \
#'' using 78 title "" linecolor rgb "#FF0000", \
#'' using 79 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "7_17", '' using 80 title "" linecolor rgb "#000000", \
#'' using 81 title "" linecolor rgb "#FFFFFF", \
#'' using 82 title "" linecolor rgb "#BEBEBE", \
#'' using 83 title "" linecolor rgb "#90EE90", \
#'' using 84 title "" linecolor rgb "#FF0000", \
#'' using 85 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "7_25", '' using 86 title "" linecolor rgb "#000000", \
#'' using 87 title "" linecolor rgb "#FFFFFF", \
#'' using 88 title "" linecolor rgb "#BEBEBE", \
#'' using 89 title "" linecolor rgb "#90EE90", \
#'' using 90 title "" linecolor rgb "#FF0000", \
#'' using 91 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "9_11", '' using 92 title "" linecolor rgb "#000000", \
#'' using 93 title "" linecolor rgb "#FFFFFF", \
#'' using 94 title "" linecolor rgb "#BEBEBE", \
#'' using 95 title "" linecolor rgb "#90EE90", \
#'' using 96 title "" linecolor rgb "#FF0000", \
#'' using 97 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "9_19", '' using 98 title "" linecolor rgb "#000000", \
#'' using 99 title "" linecolor rgb "#FFFFFF", \
#'' using 100 title "" linecolor rgb "#BEBEBE", \
#'' using 101 title "" linecolor rgb "#90EE90", \
#'' using 102 title "" linecolor rgb "#FF0000", \
#'' using 103 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "9_27", '' using 104 title "" linecolor rgb "#000000", \
#'' using 105 title "" linecolor rgb "#FFFFFF", \
#'' using 106 title "" linecolor rgb "#BEBEBE", \
#'' using 107 title "" linecolor rgb "#90EE90", \
#'' using 108 title "" linecolor rgb "#FF0000", \
#'' using 109 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "10_12", '' using 110 title "" linecolor rgb "#000000", \
#'' using 111 title "" linecolor rgb "#FFFFFF", \
#'' using 112 title "" linecolor rgb "#BEBEBE", \
#'' using 113 title "" linecolor rgb "#90EE90", \
#'' using 114 title "" linecolor rgb "#FF0000", \
#'' using 115 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "10_20", '' using 116 title "" linecolor rgb "#000000", \
#'' using 117 title "" linecolor rgb "#FFFFFF", \
#'' using 118 title "" linecolor rgb "#BEBEBE", \
#'' using 119 title "" linecolor rgb "#90EE90", \
#'' using 120 title "" linecolor rgb "#FF0000", \
#'' using 121 title "" linecolor rgb "#000000" fill pattern 0, \
#newhistogram "10_28", '' using 122 title "" linecolor rgb "#000000", \
#'' using 123 title "" linecolor rgb "#FFFFFF", \
#'' using 124 title "" linecolor rgb "#BEBEBE", \
#'' using 125 title "" linecolor rgb "#90EE90", \
#'' using 126 title "" linecolor rgb "#FF0000", \
#'' using 127 title "" linecolor rgb "#000000" fill pattern 0
