# simply normailzed the size and lifetime information
import sys
import math

sizes = []
lifetimes = []
max_size = 0.0
file = open(sys.argv[1], "r")
line = file.readline()
while line:
    size = float(line.split()[0])
    sizes.append(size)
    if max_size < size:
        max_size = size
    lifetime = float(line.split()[1])
    lifetimes.append(lifetime)
    line = file.readline()


outfile = open("../nor_size_lifetime/" + sys.argv[1], "w")
for i in range(0, len(sizes)):
    outfile.write( str((sizes[i] / max_size)) + "\t" + str(lifetimes[i]) + "\n")


