import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from numpy.random import randn
import numpy as np

start_time = []
color = []
axs = []
var_total = 3
fig = plt.figure()

read_time_list = []
write_time_list = []
read_offset_list = []
write_offset_list = []

#plt.plot (randn(50).cumsum(), 'k')
def init_color ():
    color.append("#0000FF")
    color.append("#8A2BE2")
    color.append("#A52A2A")
    color.append("#5F9EA0")
    color.append("#7FFF00")
    color.append("#000000")
    
    
def plt_one (line):
    if "mmap" in line:
        print ("add start time")
        time_start = line.find("time")
        name_start = line.find("name")
        time_loc = line.find("time") + len("time:")
        name_loc = line.find("name") + len("name:") + len("file_")
        
        time = int(line[time_loc:name_start])
        name_index = int(line[name_loc:])
        #print (name_index)
        start_time.append(time)
        ax = fig.add_subplot(var_total, 1, name_index)
        ax.set_title("Variable " + str(name_index) )
        axs.append(ax)

        read_time = []
        write_time = []
        read_offset = []
        write_offset = []
        
        read_time_list.append(read_time)
        write_time_list.append(write_time)
        read_offset_list.append(read_offset)
        write_offset_list.append(write_offset)
 
    if "Trace" in line:
        time_start = line.find("Trace")
        name_start = line.find("name")
        offset_start = line.find("offset")
        
        time_loc = line.find("Trace") + len("Trace:")
        name_loc = line.find("name") + len("name:") + len("file_")
        offset_loc = line.find("offset") + len("offset:")
        
        if "Write" in line:
            name_end = line.find("Write")
        if "Read" in line:
            name_end = line.find("Read")
        
        time = int(line[time_loc:name_start])
        name_index = int(line[name_loc:name_end])
        offset = int(line[offset_loc:])
        if "Write" in line:
            #plt.scatter(time - start_time[name_index-1], offset, color=color[name_index-1] )
            #axs[name_index-1].plot((time - start_time[name_index-1])/1e6, offset, '.', color=color[name_index-1], markersize=5)
            write_time_list[name_index-1].append((time - start_time[name_index-1]) / 1e6)
            write_offset_list[name_index-1].append(offset)
        if "Read" in line:
            #plt.scatter(time - start_time[name_index-1], offset, color=color[(name_index-1)*2] )
            #axs[name_index-1].plot((time - start_time[name_index-1])/1e6, offset, '.', color=color[(name_index-1)+var_total], markersize=5)
            read_time_list[name_index-1].append((time - start_time[name_index-1]) / 1e6)
            read_offset_list[name_index-1].append(offset)

def draw_pic():
    for i in range(0, var_total):
        axs[i].plot(write_time_list[i], write_offset_list[i], '.',  color=color[i], markersize=2)
        axs[i].plot(read_time_list[i], read_offset_list[i], '.', color=color[i+var_total], markersize=2)
        
def add_label():
    for i in range(0, 2 * len(start_time)):
        if i >= len(start_time):
            var_num = i - len(start_time) + 1
        else:
            var_num = i + 1
        print(var_num)
        if i >= len(start_time):
            #patch = mpatches.Patch(color=color[i], label=str(var_num)+"_write")
            b1 = axs[var_num-1].bar([0], [0], width=0.4, color=color[i], label="read")
        else:
            #patch = mpatches.Patch(color=color[i], label=str(var_num)+"_read")
            b1 = axs[var_num-1].bar([0], [0], width=0.4, color=color[i], label="write")
        #plt.legend(handles=[patch],frameon='false', loc="upper right", shadow="true", mode="expand", fancybox="true")
        axs[var_num-1].legend(loc='best', fancybox=True, shadow=True)

init_color()
for i in range(0, var_total):
    print(i)
    f = open("mm-trace-file_" + str(i+1))
    line = f.readline()
    while line:
        plt_one(line)
        line = f.readline()

draw_pic()
add_label()
plt.xlabel("Time(s)")
plt.ylabel("Access offset (Page)")
#plt.legend(loc="best", shadow="true", bbox_to_anchor=(1, 1))
plt.show()
plt.savefig('trace1.png', dpi=1000 )
plt.savefig('trace.pdf', dpi=1000 )
