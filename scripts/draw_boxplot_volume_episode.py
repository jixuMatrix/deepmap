import matplotlib.pyplot as plt
from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes, clf


plt.figure()
ax = axes()
hold(True)
color = []
xticks_array = [1.5, 3.5, 5.5, 7.5]
xticks_name = ["1", "5", "10", "100"]

def GetData(file_name, array):
    f = open(file_name, "r");
    line = f.readline();
    while line:
        line = line.strip("\n");
        if "nan" in line:
            line = "0"
        array.append(float(line))
        line = f.readline()

sci_1_episode = []
sci_5_episode = []
sci_10_episode = []
sci_100_episode = []

other_1_episode = []
other_5_episode = []
other_10_episode = []
other_100_episode = []

def init_color ():
    color.append("black")
    color.append("blue")
    color.append("#A52A2A")
    color.append("#5F9EA0")
    color.append("#7FFF00")
    color.append("#000000")


def setBoxColors(bp):
    setp(bp['boxes'][0], color= color[0])
    setp(bp['caps'][0], color=color[0])
    setp(bp['caps'][1], color=color[0])
    setp(bp['whiskers'][0], color=color[0])
    setp(bp['whiskers'][1], color=color[0])
    #setp(bp['fliers'][0], color=color[0])
    #setp(bp['fliers'][1], color=color[0])
    setp(bp['medians'][0], color='red')
'''
    setp(bp['boxes'][1], color=color[1])
    setp(bp['caps'][2], color=color[1])
    setp(bp['caps'][3], color=color[1])
    setp(bp['whiskers'][2], color=color[1])
    setp(bp['whiskers'][3], color=color[1])
    #setp(bp['fliers'][2], color=color[1])
    #setp(bp['fliers'][3], color=color[1])
    setp(bp['medians'][1], color=color[1])

    setp(bp['boxes'][2], color=color[2])
    setp(bp['caps'][4], color=color[2])
    setp(bp['caps'][5], color=color[2])
    setp(bp['whiskers'][4], color=color[2])
    setp(bp['whiskers'][5], color=color[2])
    #setp(bp['fliers'][2], color=color[2])
    #setp(bp['fliers'][3], color=color[2])
    setp(bp['medians'][2], color=color[2])

    setp(bp['boxes'][3], color=color[3])
    setp(bp['caps'][6], color=color[3])
    setp(bp['caps'][7], color=color[3])
    setp(bp['whiskers'][6], color=color[3])
    setp(bp['whiskers'][7], color=color[3])
    #setp(bp['fliers'][2], color=color[2])
    #setp(bp['fliers'][3], color=color[2])
    setp(bp['medians'][3], color=color[3])

    setp(bp['boxes'][4], color=color[4])
    setp(bp['caps'][8], color=color[4])
    setp(bp['caps'][9], color=color[4])
    setp(bp['whiskers'][8], color=color[4])
    setp(bp['whiskers'][9], color=color[4])
    #setp(bp['fliers'][2], color=color[2])
    #setp(bp['fliers'][3], color=color[2])
    setp(bp['medians'][4], color=color[4])
'''
GetData("/home/matrix/episode/sci/read_1_res", sci_1_episode);        
GetData("/home/matrix/episode/sci/read_5_res", sci_5_episode);        
GetData("/home/matrix/episode/sci/read_10_res", sci_10_episode);        
GetData("/home/matrix/episode/sci/read_100_res", sci_100_episode);        

GetData("/home/matrix/episode/others/read_1_res", other_1_episode);        
GetData("/home/matrix/episode/others/read_5_res", other_5_episode);        
GetData("/home/matrix/episode/others/read_10_res", other_10_episode);        
GetData("/home/matrix/episode/others/read_100_res", other_100_episode);        

init_color()

legend_array = []

hA, = plot([0,0], 'black')
legend_array.append(hA)

hA, = plot([0,0], 'blue')
legend_array.append(hA)


bp = plt.boxplot(sci_1_episode, positions = range(1, 2), showfliers = False, widths = 0.8)
#setBoxColors(bp)
setBoxColors(bp)
bp = plt.boxplot(sci_5_episode, positions = range(3, 4), showfliers = False, widths = 0.8)
setBoxColors(bp)
bp = plt.boxplot(sci_10_episode, positions = range(5, 6), showfliers = False, widths = 0.8)
setBoxColors(bp)
bp = plt.boxplot(sci_100_episode, positions = range(7, 8), showfliers = False, widths = 0.8)
setBoxColors(bp)


bp = plt.boxplot(other_1_episode, positions = range(2, 3), showfliers = False, widths = 0.8)
bp = plt.boxplot(other_5_episode, positions = range(4, 5), showfliers = False, widths = 0.8)
bp = plt.boxplot(other_10_episode, positions = range(6, 7), showfliers = False, widths = 0.8)
bp = plt.boxplot(other_100_episode, positions = range(8, 9), showfliers = False, widths = 0.8)
#setBoxColors(bp)

plt.legend(legend_array, ["sci", "other"], fontsize = 24)
plt.xlim(0.5, 8.5)
plt.yticks(fontsize = 18)
plt.ylabel("CoV", fontsize = 18)
plt.xlabel("Episode size(billions inst)", fontsize = 18)
ax.set_xticks(xticks_array)
ax.set_xticklabels(xticks_name, fontsize = 18)               
plt.savefig("test.eps")
