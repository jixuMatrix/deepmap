import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes, clf
from numpy.random import randn
import numpy as np
import struct
import sys
import os
import math

color = []
var_total = 28

root_dir = sys.argv[1]
out_dir = sys.argv[2]

# the variable draw on the figures start from 1. the same order in the array.
variable_list = [1, 2, 3, 7, 15, 23, 8, 16, 24, 12, 20, 28]
#variable_list = [1, 2, 3]
# the name draw on the figures
name_list = ["1_1", "2_1", "3_1", "4_1", "4_2", "4_3", "5_1", "5_2", "5_3", "6_1", "6_2", "6_3"]
# the threshold for the sequentail part in the variable
MIN_SEQ = 5

var_length = 0
total_access = 0L
last_access = 0L
last_operation = 0
in_sequential = 0
seq_begin = 0L
seq_begin_time = 0L
seq_begin_offset = 0L
sequential_num = 0L
sequential_total = 0L
reuse_count = 0L
read_count = 0L

test_count = 0   #total count of tests, typically 3, ref, test, train 

access_off = []  #access offset distribution
access_dis = []  #access distance distribution
access_loc = []
reuse_dis = []   #reuse distance distribution
avg_resue_dis = []
reuse_stack = []

#three list of the distribution
access_offset_list = []
access_distance_list = []
reuse_distance_list = []

list_obeject = []
object_in_process = 0
vobject_id = 0
total_object_num = 0

start_time = 0.0  #single variable start time
end_time = 0.0    #single variable end time
zero_time = 0.0   #tracing start time
final_time = 0.0  #tracing end time
#hit_8 = 0
#hit_32 = 0
#hit_256 = 0
#hit_1024 = 0

class LRUCache():
    def __init__(self, size):
        self.size = size
        self.data = []

    def access(self, offset):
    	if offset in self.data:
            self.data.remove(offset)
            self.data.append(offset)
            return 1
        else:
            if (len(self.data) == self.size):
                self.data.remove(iter(self.data).next())
                self.data.append(offset)
            else:
                self.data.append(offset)
            return 0

    def clear(self):
        self.data = []

#lru_8 = LRUCache(8)
#lru_32 = LRUCache(32)
#lru_256 = LRUCache(256)
#lru_1024 = LRUCache(1024)

#plt.plot (randn(50).cumsum(), 'k')
def init_color ():
    color.append("#0000FF")
    color.append("#8A2BE2")
    color.append("#A52A2A")
    color.append("#5F9EA0")
    color.append("#7FFF00")
    color.append("#000000")


def setBoxColors(bp):
    setp(bp['boxes'][0], color= color[0])
    setp(bp['caps'][0], color=color[0])
    setp(bp['caps'][1], color=color[0])
    setp(bp['whiskers'][0], color=color[0])
    setp(bp['whiskers'][1], color=color[0])
    #setp(bp['fliers'][0], color=color[0])
    #setp(bp['fliers'][1], color=color[0])
    setp(bp['medians'][0], color=color[0])

    setp(bp['boxes'][1], color=color[1])
    setp(bp['caps'][2], color=color[1])
    setp(bp['caps'][3], color=color[1])
    setp(bp['whiskers'][2], color=color[1])
    setp(bp['whiskers'][3], color=color[1])
    #setp(bp['fliers'][2], color=color[1])
    #setp(bp['fliers'][3], color=color[1])
    setp(bp['medians'][1], color=color[1])

    setp(bp['boxes'][2], color=color[2])
    setp(bp['caps'][4], color=color[2])
    setp(bp['caps'][5], color=color[2])
    setp(bp['whiskers'][4], color=color[2])
    setp(bp['whiskers'][5], color=color[2])
    #setp(bp['fliers'][2], color=color[2])
    #setp(bp['fliers'][3], color=color[2])
    setp(bp['medians'][2], color=color[2])

    setp(bp['boxes'][3], color=color[3])
    setp(bp['caps'][6], color=color[3])
    setp(bp['caps'][7], color=color[3])
    setp(bp['whiskers'][6], color=color[3])
    setp(bp['whiskers'][7], color=color[3])
    #setp(bp['fliers'][2], color=color[2])
    #setp(bp['fliers'][3], color=color[2])
    setp(bp['medians'][3], color=color[3])

    setp(bp['boxes'][4], color=color[4])
    setp(bp['caps'][8], color=color[4])
    setp(bp['caps'][9], color=color[4])
    setp(bp['whiskers'][8], color=color[4])
    setp(bp['whiskers'][9], color=color[4])
    #setp(bp['fliers'][2], color=color[2])
    #setp(bp['fliers'][3], color=color[2])
    setp(bp['medians'][4], color=color[4])

# this is variable id ....
def get_object_id(f_list):
    global list_obeject
    global total_object_num
    global object_id
    found = 0
    for item in list_obeject:
        if item == f_list:
            object_id = list_obeject.index(item) + 1
            found = 1
    if found == 0:
        total_object_num = total_object_num + 1
        object_id = total_object_num
        list_obeject.append(f_list)

def get_length(var_num, root_dir):
    global var_length
    global access_dis
    global access_off
    global access_loc
    global reuse_dis
    global avg_reuse_dis
    global reuse_stack
    global start_time
    global end_time
    global zero_time
    global final_time
    f_list = []
    trace_file = open(root_dir + "/call-trace-mmap", "r")
    line = trace_file.readline()
    ptr_string = ""
    find_end = 0
    while line:
        if zero_time == 0 and "[Start]" in line:
            time_start = line.find("time:") + len("time:")
            time_string = line[time_start:]
            zero_time = float(time_string)

        if final_time == 0 and "[End]" in line:
            time_start = line.find("time:") + len("time:")
            time_string = line[time_start:]
            final_time = float(time_string)
            
        if "[Trace]" in line and "malloc" in line:
            loc1 = line.find("malloc") + len("malloc:")
            loc2 = line.find("ptr")
            object_in_process = 0
            if var_num == int(line[loc1:loc2]):
                object_in_process = 1
                loc1 = line.find("size") + len("size:")
                loc2 = line.find("times")
                var_length = int(line[loc1:loc2])
		var_length = int(var_length / (1024*4)) + 2
                access_dis = [0] * var_length
                access_off = [0] * var_length
                access_loc = [-1] * var_length
                reuse_dis = [0] * var_length
                avg_reuse_dis = [0] * var_length

                time_start = line.find("times:") + len("times:")
                time_string = line[time_start:]
                start_time = float(time_string)
                
                ptr_start = line.find("ptr:") + len("ptr:")
                ptr_end = line.find("size:")
                ptr_string = line[ptr_start:ptr_end]
                
		print("page_length " + str(var_length))

        if "[Trace]" in line and "free" in line and find_end == 0:
            ptr_start = line.find("ptr:") + len("ptr:")
            ptr_end = line.find("times:")
            if ptr_string == line[ptr_start:ptr_end]:
                #print("find free!" + ptr_string)
                time_start = line.find("times:") + len("times:")
                time_string = line[time_start:]
                end_time = float(time_string)
                find_end = 1
                
        if "[Call]" in line and "Execution path:" not in line and object_in_process:
            function_name_start = line.find("(") + 1
            function_name_end = line.find(")") 
            function_name = line[function_name_start:function_name_end]
            f_list.append(function_name)
            
        if find_end == 0 and "[End]" in line:
            #print("Until the End!")
            time_start = line.find("time:") + len("time:")
            time_string = line[time_start:]
            end_time = float(time_string)
    
        line = trace_file.readline()
    #print("F_list", f_list)
    get_object_id(f_list)
    trace_file.close()

def find_pre(lru_pointer, pos, head):
    header = head
    while lru_pointer[header] != -1:
        if lru_pointer[header] == pos:
            return header
        header = lru_pointer[header]
    

def hit_lru(offset, size):
    if offset in lru_array:
        pos = lru_array.find(offset)
        pre_pos = find_pre(lru_pointer, pos, header)
        lru_pointer[pre_pos] = lru_pointer[pos]
        lru_pointer[pos] = header
        header = pos
    else:
        tail = tail_32
    #unfinished


def plt_one (time_string, offset_string, fout):
    global last_access, last_operation
    global total_access
    global in_sequential
    global sequential_num
    global sequential_total
    global reuse_count, read_count
    global reuse_stack
    global seq_begin, seq_begin_time, seq_begin_offset
#    global hit_8, hit_32, hit_256, hit_1024

    time, = struct.unpack("I", time_string)
    offset, = struct.unpack("I", offset_string)
    time = long(time) + (long(offset & 0xe0000000) << 3)
    operation = (offset & 0x10000000) 
    offset = long(offset & 0x0fffffff)

    #print (offset, time)
    if offset == 0x0fffffff:
        return
    
    if operation == last_operation and offset == last_access:
        return
    #fout.write("All: \t" + str(operation) + "\t" + str(offset) + "\n")
    if in_sequential == 1:  #forwards
        if last_access + 1 != offset and last_access != offset:
            in_sequential = 0
            if sequential_num > MIN_SEQ:
                fout.write("S: " + str(sequential_num) + "\t" 
                           + str(seq_begin) + "\t" + str(seq_begin_time)
                           + "\t" + str(seq_begin_offset) + "\t" + str(total_access)
                           + "\t" + str(time) + "\t" + str(offset) + "\n")
                sequential_total += sequential_num
        else:
            sequential_num += 1
            
    if in_sequential == 2:  #backwards
        if last_access - 1 != offset and last_access != offset:
            in_sequential = 0
            if sequential_num > MIN_SEQ:
                fout.write("S: " + str(sequential_num) + "\t" 
                           + str(seq_begin) + "\t" + str(seq_begin_time)
                           + "\t" + str(seq_begin_offset) + "\t" + str(total_access)
                           + "\t" + str(time) + "\t" + str(offset) + "\n")
                sequential_total += sequential_num
        else:
            sequential_num += 1
 
    if in_sequential == 0:  # not in sequential part
        if last_access + 1 == offset:
            in_sequential = 1
        if last_access - 1 == offset:
            in_sequential = 2
        if last_access + 1 == offset or last_access - 1 == offset: 
            seq_begin = total_access
            seq_begin_time = time
            seq_begin_offset = offset
            sequential_num = 0
    
    access_off[offset + 1] += 1
    access_dis[abs(offset - last_access)] += 1

    #if access_loc[offset + 1] != -1 and last_access != offset:
        #reuse_dis[offset + 1] = total_access - access_loc[offset + 1]
    #    reuse_dis[offset + 1] += len(reuse_stack) - reuse_stack.index(offset) - 1
    #    reuse_stack.remove(offset)
    #    reuse_count += 1

    #reuse_stack.append(offset)
    access_loc[offset + 1] = total_access
        
    last_access = offset
    if operation == 0:
        read_count += 1
    total_access += 1
    last_operation = operation

#    hit_8 += lru_8.access(offset)
#    hit_32 += lru_32.access(offset)
#    hit_256 += lru_256.access(offset)
#    hit_1024 += lru_1024.access(offset)

def draw_pic(array):
    plt.plot(range(1, var_length+1), array, '.',  color=color[3], markersize=4)
    #plt.plot(range(1, var_length+1), read_offset, '.-', color=color[2], markersize=2)
        
def add_label():
    b1 = plt.bar([0], [0], width = 0.4, color = color[2], label="read")
    b1 = plt.bar([0], [0], width = 0.4, color = color[1], label="write")
    plt.legend(loc = 'best', fancybox = True, shadow = True)

def work(dirname):
    global color
    global var_total
    global root_dir
    global out_dir
    global variable_list
    global name_list
    global MIN_SEQ
    global var_length
    global total_access
    global last_access
    global last_operation
    global in_sequential 
    global seq_begin
    global seq_begin_time
    global seq_begin_offset
    global sequential_num
    global sequential_total
    global reuse_count
    global read_count
    global test_count
    global access_off
    global access_dis
    global access_loc
    global reuse_dis 
    global avg_resue_dis
    global reuse_stack
    global access_offset_list
    global access_distance_list
    global reuse_distance_list 
    global list_obeject
    global object_in_process
    global vobject_id
    global total_object_num
    global start_time
    global end_time
    global zero_me
    global final_time 
    
    init_color()
    ftotal = open(sys.argv[2] + "/out_total_" + dirname + ".data", "w")
    ftotal.write("Object ID\tVariable ID\tTotal pages \tTotal Access \tAverge accesses per page \tAverage distances\tTotal reuse times\tAverage reuse distances\tRead ratio\t Sequentail Ratio \tNormalized Standard deviation of access offset distribution \tskewness of access offset distribution\n")

    for i in range(0, var_total):
        print("processing... " + str(i))
                
        var_length = 0L
        total_access = 0L
        last_access = 0L
        last_operation = 0L
        in_sequential = 0L
        seq_begin = 0L
        seq_begin_time = 0L
        seq_begin_offset = 0L
        sequential_num = 0L
        sequential_total = 0L
        reuse_count = 0L
        read_count = 0L
        
        access_off = []
        access_dis = []
        access_loc = []
        reuse_dis = []
        avg_resue_dis = []
        reuse_stack = []
        
        if not os.path.isfile(root_dir + "/" + dirname + "/mm-trace-file_" + str(i + 1)):
            break
        f = open(root_dir + "/" + dirname + "/mm-trace-file_" + str(i + 1), 'rb')
        fout = open(out_dir + "/out_" + dirname + "_" + str(i + 1) + ".data", "w")
        reuse_stack = []

        get_length(i + 1, root_dir + "/" + dirname)
        last_access = -1
        total_access = 0
    
        #    lru_8.clear()
        #    lru_32.clear()
        #    lru_256.clear()
        #    lru_1024.clear()
        
        #    hit_8 = 0
        #    hit_32 = 0
        #    hit_256 = 0
        #    hit_1024 = 0

        time_string = f.read(4)
        offset_string = f.read(4)

        #time_string = f.read(4)
       #offset_string = f.read(4)
	
        while time_string:
            plt_one(time_string, offset_string, fout)
            time_string = f.read(4)
            offset_string = f.read(4)

        for j in range(1, var_length):
            if access_off[j] > 1:
                avg_reuse_dis[j] = reuse_dis[j] / (access_off[j] - 1) 
            else:
                avg_reuse_dis[j] = -1
                
        print("begin to draw")
        
        if len(access_distance_list) < i + 1:
            access_offset_list.append([])
            access_distance_list.append([])
            reuse_distance_list.append([])
        
        #print access_dis 
        
        draw_pic(access_dis)
        plt.xlabel("Access distance(Page)")
        plt.ylabel("Numbers")
        plt.legend(loc = "best", shadow = "true", bbox_to_anchor = (1, 1))
        plt.savefig(out_dir + "/distance_" + str(i + 1) + ".png", dpi = 1000 )
        plt.clf()
        access_distance_list[i].append(access_dis)
        
        draw_pic(access_off)
        plt.xlabel("Access Offset(Page)")
        plt.ylabel("Numbers")
        plt.legend(loc = "best", shadow = "true", bbox_to_anchor = (1, 1))
        plt.savefig(out_dir + "/offset_" + str(i + 1) + ".png", dpi = 1000 )
        plt.clf()
        access_offset_list[i].append(access_off)
        
        draw_pic(reuse_dis)
        plt.xlabel("Reuse distance(Page)")
        plt.ylabel("Numbers")
        plt.legend(loc = "best", shadow = "true", bbox_to_anchor = (1, 1))
        plt.savefig(out_dir + "/reuse_dis_" + str(i + 1) + ".png", dpi = 1000 )
        plt.clf()
        reuse_distance_list[i].append(reuse_dis)
    
        total_dis = 0
        total_reuse_dis = 0
        avg_access_off = 0.0
        for j in range(1, var_length):
            total_dis += access_dis[j] * j
        for j in range(1, var_length):
            if reuse_dis[j] != 0:
                total_reuse_dis += reuse_dis[j]

        # calculate the standrand deviation
        for j in range(1, var_length):
            avg_access_off += access_off[j]
        avg_access_off = avg_access_off / (var_length - 1)

        deviation = 0.0
        for j in range(1, var_length):
            deviation += (access_off[j] - avg_access_off) * (access_off[j] - avg_access_off)
        deviation = math.sqrt(deviation / (var_length - 1))
    
        # calculate the skewness
        skewness = 0.0
        for j in range(1, var_length):
            skewness += (access_off[j] - avg_access_off) * (access_off[j] - avg_access_off) *  (access_off[j] - avg_access_off)
        if not deviation == 0.0:
            skewness = skewness / (var_length - 1) / (deviation * deviation * deviation)
        else:
            skewness = 0.0

        if reuse_count == 0:
            reuse_count = 1
        
        #    start_time = start_time - zero_time
        #    end_time = end_time - zero_time
    
        fout.write("Object ID:" + str(object_id) + "\n")
        fout.write("Total Pages:" + str(var_length - 1) + "\n")
        fout.write("Total Access:" + str(total_access) + "\n")
        fout.write("Averge access times:" + str(float(total_access) / (var_length - 1)) + "\n")
        fout.write("Total reuse times:" + str(reuse_count) + "\n")
        fout.write("Average distance of accesses:" + str(float(total_dis) / (total_access - 1)) + "\n")
        fout.write("Average reuse distance of accesses:" + str(float(total_reuse_dis) / (reuse_count) * 100.0) + "\n")
        fout.write("Read ratio:" + str(float(read_count) / (total_access) * 100) + "\n")
        fout.write("Sequential ratio:" + str(float(sequential_total) / (total_access)) + "\n")
        fout.write("Normalized Standard deviation of access offset distribution:" + str(deviation / avg_access_off) + "\n")
        fout.write("Skewness of access offset distribution:" + str(skewness) + "\n")
        fout.write("Start time:" + str((start_time - zero_time) * 100 / (final_time - zero_time)) + "%\n")
        fout.write("End time:" + str((end_time - zero_time) * 100 / (final_time - zero_time)) + "%\n")
        fout.write("Access rate:" + str(float(total_access) / (end_time - start_time)) + "\n")

        ftotal.write( str(i + 1) + "\t" + str(object_id) + "\t" + str(var_length - 1) + "\t" + str(total_access) + "\t"
                      + str(float(total_access) / (var_length - 1)) + "\t" 
                      + str(float(total_dis) / (total_access - 1)) + "\t"
                      + str(reuse_count) + "\t"
                      + str(float(total_reuse_dis) / (reuse_count)) + "\t" 
                      + str(float(read_count) / (total_access) * 100) + "\t"
                      + str(float(sequential_total) / (total_access) * 100.0) + "\t" 
                      + str(deviation / avg_access_off) + "\t" + str(skewness) + "\t"
                      + str((start_time - zero_time) * 100 / (final_time - zero_time)) + "%\t" 
                      + str((end_time - zero_time) * 100 / (final_time - zero_time)) + "%\t"
                      + str(float(total_access) / (end_time - start_time)) + "\n")

        '''
        fout.write("8 Pages Cache hit:" + str(hit_8) + "\n")
        fout.write("hit rate:" + str(float(hit_8) * 100 / total_access) + "%\n")

        fout.write("32 Pages Cache hit:" + str(hit_32) + "\n")
        fout.write("hit rate:" + str(float(hit_32) * 100 / total_access) + "%\n")

        fout.write("256 Pages Cache hit:" + str(hit_256) + "\n")
        fout.write("hit rate:" + str(float(hit_256) * 100 / total_access) + "%\n")

        fout.write("1024 Pages Cache hit:" + str(hit_1024) + "\n")
        fout.write("hit rate:" + str(float(hit_1024) * 100 / total_access) + "%\n")
        '''
        fout.close()
        f.close()

    ftotal.close()

def draw_boxplot(dirnames):
    global test_count
    
    xticks_array = []

    for i in range(0, len(variable_list)):
        xticks_array.append( i * test_count + test_count / 2.0 )
    
    
    for k in range(0, 3):
        plt.figure()
        ax = axes()
        hold(True)

        legend_array = []
        for i in range(0, test_count):
            hA, = plot([1,1], color[i])
            legend_array.append(hA)
   
        for i in range(0, len(variable_list)):
            if k == 0: # access_offset_list
                bp = plt.boxplot(access_offset_list[variable_list[i] - 1], positions = range(i * test_count, (i + 1) * test_count), showmeans = True, meanline = True, meanprops = dict(linestyle='-', linewidth = 2, color = 'black'),  showfliers = False, widths = 0.8)
                print("access_offset_list:", i)
                print(access_offset_list[variable_list[i] - 1])
            elif k == 1: #access_distance_list
                bp = plt.boxplot(access_distance_list[variable_list[i] - 1], positions = range(i * test_count, (i + 1) * test_count), showmeans = True, meanline = True, meanprops = dict(linestyle='-', linewidth = 2, color = 'black'), showfliers = False, widths = 0.8)
                print("access_distance_list:", i)
                print(access_distance_list[variable_list[i] - 1])
            elif k == 2: #reuse_dista
                #print variable_list[i] - 1
                #print len(reuse_distance_list[variable_list[i] - 1])
                bp = plt.boxplot(reuse_distance_list[variable_list[i] - 1], positions = range(i * test_count, (i + 1) * test_count), showmeans = True, meanline = True, meanprops = dict(linestyle='-', linewidth = 2, color = 'black'), showfliers = False,  widths = 0.8)
                print("reuse_distance_list:", i)
                print(reuse_distance_list[variable_list[i] - 1])
            #bp = boxplot(access_offset_list[0], positions = [i], widths = 0.6)
            #bp = boxplot(access_offset_list[1], positions = [i+1], widths = 0.6)
        
            setBoxColors(bp)
    
        plt.xlim(-1, len(variable_list) + 1)
        #plt.ylim(0, 60)
        ax.set_xticklabels(name_list)
        ax.set_xticks(xticks_array)
         
        #plt.legend(legend_array, dirnames)
  
        for i in range(0, test_count):
            legend_array[i].set_visible(False)
    
        if k == 0:
            plt.savefig("access_offset_figure.eps")
        if k == 1:
            plt.savefig("access_distance_figure.eps")
        if k == 2:
            plt.savefig("resue_distance_figure.eps")
        plt.clf()
        
if out_dir == "":
    out_dir = os.getcwd()

for parent, dirnames, filename in os.walk(root_dir):
    dirnames.sort()
    test_count = len(dirnames)
    for dirname in dirnames:
        work(dirname)
    break
    
draw_boxplot(dirnames)
