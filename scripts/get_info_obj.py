import sys
import math

CHANGE_RATIO = 5
variable_out_file = open(sys.argv[1] + "/variable_out_obj")
line = variable_out_file.readline()

#read = [[0.0] * 10] * 10
var_belong = []
obj_belong = []
var_exsit = [0] * 10
size = [([0] * 3) for i in range(10)]
density = [([0.0] * 3) for i in range(10)]
seq = [([0.0] * 3) for i in range(10)]
read = [([0.0] * 3) for i in range(10)]
#while line:
#    line = variable_out_file.readline()

#print read
while line:
    
    loc = line.find("_")
    #print line[:loc]
    var_exsit[int(line[:loc]) - 1] = 1
   
    var_belong.append(int(line[:loc]) - 1)
    obj_belong.append(int(line[loc+1:loc+2]) - 1)
    line = variable_out_file.readline()
    
data_file = open(sys.argv[1] + "/top10-malloctrace.out-obj")

line = data_file.readline()
while line:

    if "Size" in line:
        line = data_file.readline()
        for i in range(0, len(var_belong)):
            print i
            print line
            print line.split()[1]
            size[var_belong[i]][obj_belong[i]] = int(line.split()[1])
            line = data_file.readline()
            
    if "access_bytes_size" in line:
        line = data_file.readline()
        for i in range(0, len(var_belong)):
            density[var_belong[i]][obj_belong[i]] = float(line.split()[1])
            line = data_file.readline()

    if "seq_ratio_volume" in line:
        line = data_file.readline()
        for i in range(0, len(var_belong)):
            seq[var_belong[i]][obj_belong[i]] = float(line.split()[1])
            line = data_file.readline()
            
    if "read_ratio_volume" in line:
        line = data_file.readline()
        for i in range(0, len(var_belong)):
            read[var_belong[i]][obj_belong[i]] = float(line.split()[1])
            line = data_file.readline()
    line = data_file.readline()
            
    
size_change = [0] * 10
multi_obj = [0] * 10
density_change = 0
density_change_fix = 0
read_change = 0
read_change_fix = 0
seq_change = 0
seq_change_fix = 0
multi_obj_var = 0
var_num = 0
size_changes = 0

file_size_sd = open("/home/xji/size_sd", "a")

print "Size"
for i in range(0, 10):
    if var_exsit[i] == 1:
        var_num += 1
        print i+1, size[i][0], size[i][1], size[i][2]
        if size[i][1] != 0:
            multi_obj_var += 1
        if size[i][2] == 0 or size[i][1] == 0:
            continue
        multi_obj[i] = 1
        if size[i][0] != size[i][1] or size[i][1] != size[i][2]:
            size_change[i] = 1
            size_changes += 1
        avg = (size[i][0] + size[i][1] + size[i][2]) / 3
        sd = 0.0
        for j in range(0, 3):
            sd += (size[i][j] - avg) * (size[i][j] - avg)
        sd = math.sqrt(sd)
        file_size_sd.write(str(sd) + "\n")
        

print "Density"

file_density_sd = open("/home/xji/density_sd", "a")

for i in range(0, 10):
    if var_exsit[i] == 1:
        print i+1, density[i][0], density[i][1], density[i][2]
        obj1 = density[i][0]
        obj2 = density[i][1]
        obj3 = density[i][2]

        if multi_obj[i] == 0:
            continue
        if obj3 == 0:
            obj3 = obj2
    
        if abs(obj1 - obj2) * 100 / max(obj1, obj2) > CHANGE_RATIO or \
        abs(obj2 - obj3)  * 100 / max(obj3, obj2) > CHANGE_RATIO or \
        abs(obj3 - obj1)  * 100 / max(obj1, obj3) > CHANGE_RATIO:
            density_change += 1
            if size_change[i] == 0:
                density_change_fix += 1
        avg = (density[i][0] + density[i][1] + density[i][2]) / 3
        sd = 0.0
        for j in range(0, 3):
            sd += (density[i][j] - avg) * (density[i][j] - avg)
        sd = math.sqrt(sd)
        file_density_sd.write(str(sd) + "\n")
            
            

print "seq"
file_seq_sd = open("/home/xji/seq_sd", "a")

for i in range(0, 10):
    if var_exsit[i] == 1:
        print i+1, seq[i][0], seq[i][1], seq[i][2]
        obj1 = seq[i][0]
        obj2 = seq[i][1]
        obj3 = seq[i][2]

        if multi_obj[i] == 0:
            continue
        
        if obj3 == 0:
            obj3 = obj2
    
        if abs(obj1 - obj2) > CHANGE_RATIO or \
        abs(obj2 - obj3) > CHANGE_RATIO or \
        abs(obj3 - obj1) > CHANGE_RATIO:
            seq_change += 1
            if size_change[i] == 0:
                seq_change_fix += 1
        avg = (seq[i][0] + seq[i][1] + seq[i][2]) / 3
        sd = 0.0
        for j in range(0, 3):
            sd += (seq[i][j] - avg) * (seq[i][j] - avg)
        sd = math.sqrt(sd)
        file_seq_sd.write(str(sd) + "\n")

        
    
print "read"
file_read_sd = open("/home/xji/read_sd", "a")

for i in range(0, 10):
    if var_exsit[i] == 1:
        print i+1, read[i][0], read[i][1], read[i][2]
        obj1 = read[i][0]
        obj2 = read[i][1]
        obj3 = read[i][2]
        if multi_obj[i] == 0:
            continue
        
        
        if obj3 == 0:
            obj3 = obj2
    
        if abs(obj1 - obj2) > CHANGE_RATIO or \
        abs(obj2 - obj3) > CHANGE_RATIO or \
        abs(obj3 - obj1) > CHANGE_RATIO:
            read_change += 1
            if size_change[i] == 0:
                read_change_fix += 1
        avg = (read[i][0] + read[i][1] + read[i][2]) / 3
        sd = 0.0
        for j in range(0, 3):
            sd += (read[i][j] - avg) * (read[i][j] - avg)
        sd = math.sqrt(sd)
        file_read_sd.write(str(sd) + "\n")
        
    

print ""
print "total", "multi", "density_change", "seq_change","read_change", "size_fix", "density_change_fix", "seq_change_fix","read_change_fix"
print var_num,  multi_obj_var, density_change, seq_change, read_change, multi_obj_var - size_changes, density_change_fix, seq_change_fix,read_change_fix
