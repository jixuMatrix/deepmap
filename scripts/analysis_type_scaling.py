#Usage: python script_name + size_file(Size.dat)
import sys
import math

top10_varids = []
top10_cates = []
cate_scaling = []
cate_fixed = []
cate_irregular = []
cate_totalsize = []

def ReadCate():
    file = open("./Cate", "r")
    line = file.readline()
    while line:
        cate = line.split()[1]
        if cate == "hashtables":
            cate_id = 1
        elif cate == "buffer":
            cate_id = 2
        elif cate == "vector":
            cate_id = 3
        elif cate == "bitmap":
            cate_id = 5
        elif cate == "index":
            cate_id = 4
        elif cate == "1D-other":
            cate_id = 3
        elif cate == "2d":
            cate_id = 6
        elif cate == "single":
            cate_id = 0
        elif cate == "2d-collection":
            cate_id = 7
        elif cate == "2d-matrix":
            cate_id = 6
        elif cate == "MD":
            cate_id = 8

        top10_varids.append(int(line.split()[0]))
        top10_cates.append(cate_id)
        #print cate_id
        #top10_consum.append(0)
        #cate_totalsize.append(0)
        line = file.readline()        
    for i in range(0, 10):
        cate_fixed.append(0)
        cate_irregular.append(0)
        cate_scaling.append(0)

ReadCate()
size_file = open(sys.argv[1], "r")

#print sys.argv[1]
line = size_file.readline()
line = size_file.readline()
#print line
i = 0
flag = 1
scaling_factor_large = 0.0
scaling_factor_mid = 0.0
scaling_factor_large_array = []
scaling_factor_mid_array = []
while line:
    big_size = int(line.split()[1])
    mid_size = int(line.split()[2])
    small_size = int(line.split()[3])
    if big_size == 0 or mid_size == 0 or small_size == 0:
        line = size_file.readline()
        i += 1
        continue
    
    if big_size <= 4096:
        line = size_file.readline()
        i += 1
        continue

    # Add temp
    #if big_size == mid_size and mid_size == small_size:
    #    line = size_file.readline()
    #    i += 1
    #    continue

    #if scaling_factor_large == 0.0:
    #scaling_factor_large = float(big_size) / mid_size
    #scaling_factor_mid = float(mid_size) / small_size
    #print top10_cates[i]
    

    if big_size == mid_size and mid_size == small_size:
        cate_fixed[top10_cates[i]] += 1
    elif big_size >= mid_size and mid_size >= small_size:
        cate_scaling[top10_cates[i]] += 1
        if abs(scaling_factor_mid - float(mid_size) / small_size) > 0.01:
            flag = 0
        if abs(scaling_factor_large - float(big_size) / mid_size) > 0.01:
            flag = 0
    else:
        cate_irregular[top10_cates[i]] += 1

    i += 1
    
    if big_size < mid_size or mid_size < small_size:
        line = size_file.readline()
        continue
    
    scaling_factor_mid_array.append(scaling_factor_mid)
    scaling_factor_large_array.append(scaling_factor_large)

    line = size_file.readline()


def FindInCate(var_id):
    global top10_varids
    for i in range(len(top10_varids)):
        if var_id == top10_varids[i]:
            return 1
    return 0

def FindCateID(var_id):
    global top10_varids
    global top10_cates
    for i in range(len(top10_varids)):
        if var_id == top10_varids[i]:
            return top10_cates[i]

#print "Var_ID", "#_of_objects", "Avg", "Footprint", "Varivance_%", "Avg_#_of_concurrent"

#print top10_cates.count(0), top10_cates.count(1), \
#    top10_cates.count(2), top10_cates.count(3), top10_cates.count(4), \
#    top10_cates.count(5), top10_cates.count(6), top10_cates.count(7), top10_cates.count(8)
#print cate_fixed[0], cate_fixed[1], cate_fixed[2],\
#    cate_fixed[3], cate_fixed[4], cate_fixed[5], \
#    cate_fixed[6], cate_fixed[7], cate_fixed[8]
#print cate_scaling[0], cate_scaling[1], cate_scaling[2],\
#    cate_scaling[3], cate_scaling[4], cate_scaling[5], \
#cate_scaling[6], cate_scaling[7], cate_scaling[8]
print cate_irregular[0], cate_irregular[1], cate_irregular[2],\
    cate_irregular[3], cate_irregular[4], cate_irregular[5], \
    cate_irregular[6], cate_irregular[7], cate_irregular[8]

#if flag == 1:
#    print "Yes"
#else:
#    print "No"

#print scaling_factor_large_array
#print scaling_factor_mid_array

#for i in range(0, len(scaling_factor_large_array)):
#    sys.stdout.write(str(scaling_factor_large_array[i]) + "\t")
#sys.stdout.write("\n")

#for i in range(0, len(scaling_factor_mid_array)):
#    sys.stdout.write(str(scaling_factor_mid_array[i]) + "\t")
#sys.stdout.write("\n")
